package com.co.bog.branch.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.co.bog.branch.util.Validacion;
import com.co.bog.branch.ws.models.Usuario;
import com.co.bog.branch.ws.service.BranchService;
import com.co.bog.branch.ws.ws;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateUserFragment extends Fragment implements View.OnClickListener {

    private static final String LOG_TAG = UpdateUserFragment.class.getSimpleName();

    private TextInputEditText txtEmail;

    private TextInputEditText txtNombreUsuario;

    private TextInputEditText txtIdentificacion;

    private TextInputEditText txtCelular;

    private MaterialButton btnActualizarUsuario;

    private MaterialButton btnCancelar;

    private View view;

    private FirebaseAuth mAuth;

    private FirebaseUser user;

    private SharedPreferences prefs;

    private SharedPreferences.Editor mEditor;

    private static final String NAME_SHARED_PREFERENCES="prefsBranchs";

    public static SwitchFragmentListener switchFragmentsUserListener;

    public UpdateUserFragment(SwitchFragmentListener switchFragmentsUserListener) {
        // Required empty public constructor
        this.switchFragmentsUserListener = switchFragmentsUserListener;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_update_user, container, false);

        prefs = getContext().getSharedPreferences(NAME_SHARED_PREFERENCES,MODE_PRIVATE);

        mEditor = prefs.edit();

        mAuth = FirebaseAuth.getInstance();

        user = mAuth.getCurrentUser();

        bindUI(view);

        return view;
    }

    private void bindUI(View view) {

        Log.d(LOG_TAG, "Usuario de firebase actual Email ::> " + user.getEmail());
        Log.d(LOG_TAG, "Usuario de firebase actual Email ::> " + user.getPhoneNumber());

        String identificacion = prefs.getString(getString(R.string.prefIdentificacion), "");
        String tipoUsuario = prefs.getString(getString(R.string.prefTipoUsuario), "");

        Log.d(LOG_TAG,"Identificacion :::>"+identificacion);

        this.txtEmail = (TextInputEditText) view.findViewById(R.id.txtEmail);
        this.txtEmail.setText(user.getEmail());
        this.txtEmail.setEnabled(false);

        this.txtNombreUsuario = (TextInputEditText) view.findViewById(R.id.txtNombreUsuario);
        this.txtNombreUsuario.setText(user.getDisplayName());

        this.txtIdentificacion = (TextInputEditText) view.findViewById(R.id.txtIdentificacion);
        this.txtIdentificacion.setText(identificacion);

        this.txtCelular = (TextInputEditText) view.findViewById(R.id.txtCelular);
        this.txtCelular.setText(user.getPhoneNumber());

        this.btnActualizarUsuario = (MaterialButton) view.findViewById(R.id.btnActualizarUsuario);
        this.btnCancelar = (MaterialButton) view.findViewById(R.id.btnCancelar);

        this.btnActualizarUsuario.setOnClickListener(this);
        this.btnCancelar.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        Log.d(LOG_TAG,"Click ::::>>" + v.getId());
        switch (v.getId()) {
            case R.id.btnActualizarUsuario:
                actualizarUsuario();
                break;
            case R.id.btnCancelar:
                switchFragmentsUserListener.onSwitchUserFragment();
                break;
        }
    }


    private void actualizarUsuario() {
        if (checkValidation()) {

            Usuario usuario = new Usuario();

            usuario.setEmail(this.txtEmail.getText().toString());
            usuario.setIdentificacion(this.txtIdentificacion.getText().toString());
            usuario.setCelular(this.txtCelular.getText().toString());
            usuario.setFirstName(this.txtNombreUsuario.getText().toString());

            BranchService service = ws.getService(getContext()).create(BranchService.class);

            Call<Object> usuarioCall = service.updateUsuario(user.getUid(), usuario);

            usuarioCall.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    if (response.isSuccessful()) {
                        Log.d(LOG_TAG, "Se actualizo exitosamente el usuario");

                        mEditor.putString(getString(R.string.prefIdentificacion), usuario.getIdentificacion());
                        mEditor.commit();

                        /*AuthCredential credential = EmailAuthProvider
                                .getCredential()*/

                        //user.updateProfile(new UserProfileChangeRequest() ins)

                        // Prompt the user to re-provide their sign-in credentials
                        mAuth.getCurrentUser().reload().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(LOG_TAG, "Recargo datos del usuario exitosamente");
                                switchFragmentsUserListener.onSwitchUserFragment();
                            }
                        });


                        //mAdapter.notifyDataSetChanged();
                        //mAdapter.notifyAll();

                    } else {
                        try {
                            int statusCode = response.code();
                            Log.e(LOG_TAG, "onResponse(): Codigo respuesta al actualizar informacion del usuario Error code = " + statusCode);

                            JSONObject jsonObj = new JSONObject(response.errorBody().string());

                            Log.e(LOG_TAG, "Json result " + jsonObj.getString("error"));

                            String errorMessage = jsonObj.getString("error");

                            Snackbar.make(view, errorMessage, Snackbar.LENGTH_LONG)
                                    .show();

                        } catch (IOException | JSONException e) {
                            Log.e(LOG_TAG, "onResponse(): Error manejado al actualizar usuario = " + e.getMessage());
                            Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                                    .show();
                        }

                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    //Toast.makeText(MotosFragment.this,"Error", Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, "Error al actualizar informacion del usuario = " + t.getMessage());
                    Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                            .show();
                }
            });


        } else {
            Snackbar.make(view, R.string.msgFormFailed, Snackbar.LENGTH_LONG)
                    .show();
        }
    }


    /**
     * @return
     */
    private boolean checkValidation() {
        boolean ret = true;

        if (!Validacion.hasText(this.txtEmail)) ret = false;
        if (!Validacion.hasText(this.txtNombreUsuario)) ret = false;
        if (!Validacion.hasText(this.txtIdentificacion)) ret = false;
        if (!Validacion.hasText(this.txtCelular)) ret = false;


        return ret;
    }

    public void backPressed() {
        switchFragmentsUserListener.onSwitchUserFragment();
    }


}
