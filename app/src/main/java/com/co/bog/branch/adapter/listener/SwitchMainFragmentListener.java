package com.co.bog.branch.adapter.listener;

public interface SwitchMainFragmentListener {

    void onSwitchFragment(int resourceId);

}
