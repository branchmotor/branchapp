package com.co.bog.branch.ws.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Vehiculo {

    private Integer IdVehiculo;



    private String IdUsuario;

    private Integer IdTaller;

    private String tipoVehiculo;

    private String placa;

    private Integer kilometraje = 0;

    private Integer modelo;

    private String color;

    private Date fechaCompra;

    private String fechaCompraText;

    private String alias;

    private List<Documento> fotos = new ArrayList<Documento>();;

    private String estado;

    private Date createdAt;

    private Date updatedAt;

    private Marca marca;

    private Usuario usuario;

    private Taller taller;

    private Documento tecnoMecanica;

    private Documento soat;

    private Documento tarjetaPropiedad;

    public Vehiculo() {

    }

    public Vehiculo(Integer idVehiculo,  String idUsuario, Integer idTaller, String tipoVehiculo, String placa, Integer kilometraje, Integer modelo, String color, Date fechaCompra, String alias, String estado, Date createdAt, Date updatedAt, Marca marca, Usuario usuario) {
        this.IdVehiculo = idVehiculo;
        this.IdUsuario = idUsuario;
        this.IdTaller = idTaller;
        this.tipoVehiculo = tipoVehiculo;
        this.placa = placa;
        this.kilometraje = kilometraje;
        this.modelo = modelo;
        this.color = color;
        this.fechaCompra = fechaCompra;
        this.alias = alias;
        this.estado = estado;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.marca = marca;
        this.usuario = usuario;
    }

    public Integer getIdVehiculo() {
        return IdVehiculo;
    }

    public void setIdVehiculo(Integer idVehiculo) {
        IdVehiculo = idVehiculo;
    }

    public String getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        IdUsuario = idUsuario;
    }

    public Integer getIdTaller() {
        return IdTaller;
    }

    public void setIdTaller(Integer idTaller) {
        IdTaller = idTaller;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Integer getKilometraje() {
        return kilometraje;
    }

    public void setKilometraje(Integer kilometraje) {
        this.kilometraje = kilometraje;
    }

    public Integer getModelo() {
        return modelo;
    }

    public void setModelo(Integer modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public String getFechaCompraText() {
        return fechaCompraText;
    }

    public void setFechaCompraText(String fechaCompraText) {
        this.fechaCompraText = fechaCompraText;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public List<Documento> getFotos() {
        return fotos;
    }

    public void setFotos(List<Documento> fotos) {
        this.fotos = fotos;
    }

    public void addPhoto(Documento documento){
        this.fotos.add(documento);
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Taller getTaller() {
        return taller;
    }

    public void setTaller(Taller taller) {
        this.taller = taller;
    }

    public Documento getTecnoMecanica() {
        return tecnoMecanica;
    }

    public void setTecnoMecanica(Documento tecnoMecanica) {
        this.tecnoMecanica = tecnoMecanica;
    }

    public Documento getSoat() {
        return soat;
    }

    public void setSoat(Documento soat) {
        this.soat = soat;
    }

    public Documento getTarjetaPropiedad() {
        return tarjetaPropiedad;
    }

    public void setTarjetaPropiedad(Documento tarjetaPropiedad) {
        this.tarjetaPropiedad = tarjetaPropiedad;
    }

    @Override
    public String toString() {
        return "Vehiculo{" +
                "IdVehiculo=" + IdVehiculo +
                ", IdUsuario='" + IdUsuario + '\'' +
                ", IdTaller=" + IdTaller +
                ", tipoVehiculo='" + tipoVehiculo + '\'' +
                ", placa='" + placa + '\'' +
                ", kilometraje=" + kilometraje +
                ", modelo=" + modelo +
                ", color='" + color + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", alias='" + alias + '\'' +
                ", estado='" + estado + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", marca=" + marca +
                ", usuario=" + usuario +
                ", taller=" + taller +
                ", fotos=" + fotos +
                '}';
    }
}
