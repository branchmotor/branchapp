package com.co.bog.branch.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.PageAdapter;
import com.co.bog.branch.adapter.listener.SwitchMainFragmentListener;
import com.co.bog.branch.fragments.CitasFragment;
import com.co.bog.branch.fragments.CotizacionFragment;
import com.co.bog.branch.fragments.CreateVehiclesFragment;
import com.co.bog.branch.fragments.DetailCitaFragment;
import com.co.bog.branch.fragments.DocumentoVehiculoFragment;
import com.co.bog.branch.fragments.GalleryCitasFragment;
import com.co.bog.branch.fragments.MotosFragment;
import com.co.bog.branch.fragments.NewCitaFragment;
import com.co.bog.branch.fragments.UpdateUserFragment;
import com.co.bog.branch.fragments.UserFragment;
import com.co.bog.branch.fragments.pictureBrowserFragment;
import com.co.bog.branch.ws.service.BranchService;
import com.co.bog.branch.ws.ws;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private SharedPreferences prefs;

    private MenuItem prevMenuItem;

    private FirebaseAuth mAuth;

    private ViewPager viewPager;

    private PageAdapter pagerAdapter;

    private MaterialToolbar myToolbar;

    private SwitchToolbarTitleListener switchTitle = new SwitchToolbarTitleListener();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // Create channel to show notifications.
            String channelId  = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }*/

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(LOG_TAG, "Key: " + key + " Value: " + value);
            }
        }

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(LOG_TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        BranchService service = ws.getService(getApplicationContext()).create(BranchService.class);

                        JsonObject usuario = new JsonObject();
                        usuario.addProperty("tokenCM", token);

                        Call<Object> usuarioCall = service.updateUsuario(mAuth.getUid(), usuario);

                        usuarioCall.enqueue(new Callback<Object>() {
                            @Override
                            public void onResponse(Call<Object> call, Response<Object> response) {
                                if (response.isSuccessful()) {
                                    Log.d(LOG_TAG, "Se actualizo exitosamente el token FCM del usuario");
                                    //mAdapter.notifyDataSetChanged();
                                    //mAdapter.notifyAll();

                                } else {
                                    int statusCode = response.code();
                                    Log.e(LOG_TAG, "onResponse(): Codigo respuesta Error code = " + statusCode);
                                }
                            }

                            @Override
                            public void onFailure(Call<Object> call, Throwable t) {
                                //Toast.makeText(MotosFragment.this,"Error", Toast.LENGTH_SHORT).show();
                                Log.e(LOG_TAG, "Error realizar update de token FCM WS = " + t.getMessage());
                                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });

        bindUI();

        prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);

    }


    @Override
    public void onBackPressed() {
        backpressed();
    }

    private void backpressed() {
        if (viewPager.getCurrentItem() == 0) {
            if (pagerAdapter.getItem(0) instanceof CreateVehiclesFragment) {
                ((CreateVehiclesFragment) pagerAdapter.getItem(0)).backPressed();
            } else {
                if (pagerAdapter.getItem(0) instanceof DocumentoVehiculoFragment) {
                    ((DocumentoVehiculoFragment) pagerAdapter.getItem(0)).backPressed();
                } else {
                    if (pagerAdapter.getItem(0) instanceof MotosFragment) {
                        finish();
                    }
                }
            }
        }

        if (viewPager.getCurrentItem() == 1) {
            viewPager.setCurrentItem(0);
        }

        if (viewPager.getCurrentItem() == 2) {
            if (pagerAdapter.getItem(2) instanceof UpdateUserFragment) {
                ((UpdateUserFragment) pagerAdapter.getItem(2)).backPressed();
            } else if (pagerAdapter.getItem(2) instanceof UserFragment) {
                viewPager.setCurrentItem(0);

            }
        }

        if (viewPager.getCurrentItem() == 3) {
            if (pagerAdapter.getItem(3) instanceof NewCitaFragment) {
                ((NewCitaFragment) pagerAdapter.getItem(3)).backPressed();
            } else {
                if (pagerAdapter.getItem(3) instanceof GalleryCitasFragment) {
                    ((GalleryCitasFragment) pagerAdapter.getItem(3)).backPressed();
                } else {
                    if (pagerAdapter.getItem(3) instanceof pictureBrowserFragment) {
                        ((pictureBrowserFragment) pagerAdapter.getItem(3)).backPressed();
                    } else {
                        if (pagerAdapter.getItem(3) instanceof CotizacionFragment) {
                            ((CotizacionFragment) pagerAdapter.getItem(3)).backPressed();
                        } else {
                            if (pagerAdapter.getItem(3) instanceof DetailCitaFragment) {
                                ((DetailCitaFragment) pagerAdapter.getItem(3)).backPressed();
                            } else {
                                if (pagerAdapter.getItem(3) instanceof CitasFragment) {
                                    viewPager.setCurrentItem(0);
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    private void bindUI() {

        myToolbar = (MaterialToolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backpressed();
            }
        });

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        final BottomNavigationView bottomNavigation = (BottomNavigationView) findViewById(R.id.navigationView);

        pagerAdapter = new PageAdapter(getSupportFragmentManager(), switchTitle);

        viewPager.setAdapter(pagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    bottomNavigation.getMenu().getItem(0).setChecked(false);
                }
                Log.d(LOG_TAG, "onPageSelected: " + position);
                bottomNavigation.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigation.getMenu().getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.navigation_motorcycle:
                        myToolbar.setTitle(R.string.motos);
                        viewPager.setCurrentItem(0);
                        break;
                    case R.id.navigation_talleres:
                        myToolbar.setTitle(R.string.talleres);
                        viewPager.setCurrentItem(1);
                        break;
                    case R.id.navigation_users:
                        myToolbar.setTitle(R.string.usuarios);
                        viewPager.setCurrentItem(2);
                        break;
                    case R.id.navigation_citas:
                        myToolbar.setTitle(R.string.citas);
                        viewPager.setCurrentItem(3);
                        break;
                }
                return true;
            }
        });


    }

    public class SwitchToolbarTitleListener implements SwitchMainFragmentListener{

        @Override
        public void onSwitchFragment(int resourceId) {
            myToolbar.setTitle(resourceId);
        }
    }







}
