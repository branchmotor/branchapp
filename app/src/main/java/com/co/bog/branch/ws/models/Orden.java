package com.co.bog.branch.ws.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Orden {

    private Integer IdOrdenTrabajo;

    private Integer IdTaller;

    private String CodigoOrden;

    private Integer IdEtapa;

    private Integer IdCita;

    private Integer IdMecanico;

    private Integer IdVehiculo;

    private Integer kilometraje;

    private JsonObject DocumentosDeja;

    private String Observaciones;

    private String estado;

    private List<Documento> documentos = new ArrayList<Documento>();

    private Mecanico mecanico;

    private Date createdAt;

    private Date updatedAt;

    public Orden(){

    }

    public Integer getIdOrdenTrabajo() {
        return IdOrdenTrabajo;
    }

    public void setIdOrdenTrabajo(Integer idOrdenTrabajo) {
        IdOrdenTrabajo = idOrdenTrabajo;
    }

    public Integer getIdTaller() {
        return IdTaller;
    }

    public void setIdTaller(Integer idTaller) {
        IdTaller = idTaller;
    }

    public String getCodigoOrden() {
        return CodigoOrden;
    }

    public void setCodigoOrden(String codigoOrden) {
        CodigoOrden = codigoOrden;
    }

    public Integer getIdEtapa() {
        return IdEtapa;
    }

    public void setIdEtapa(Integer idEtapa) {
        IdEtapa = idEtapa;
    }

    public Integer getIdCita() {
        return IdCita;
    }

    public void setIdCita(Integer idCita) {
        IdCita = idCita;
    }

    public Integer getIdMecanico() {
        return IdMecanico;
    }

    public void setIdMecanico(Integer idMecanico) {
        IdMecanico = idMecanico;
    }

    public Integer getIdVehiculo() {
        return IdVehiculo;
    }

    public void setIdVehiculo(Integer idVehiculo) {
        IdVehiculo = idVehiculo;
    }

    public Integer getKilometraje() {
        return kilometraje;
    }

    public void setKilometraje(Integer kilometraje) {
        this.kilometraje = kilometraje;
    }

    public JsonObject getDocumentosDeja() {
        return DocumentosDeja;
    }

    public void setDocumentosDeja(JsonObject documentosDeja) {
        DocumentosDeja = documentosDeja;
    }



    public String getObservaciones() {
        return Observaciones;
    }

    public void setObservaciones(String observaciones) {
        Observaciones = observaciones;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Documento> documentos) {
        this.documentos = documentos;
    }

    public void addDocumento(Documento documento){
        this.documentos.add(documento);
    }

    public Mecanico getMecanico() {
        return mecanico;
    }

    public void setMecanico(Mecanico mecanico) {
        this.mecanico = mecanico;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "Orden{" +
                "IdOrdenTrabajo=" + IdOrdenTrabajo +
                ", IdTaller=" + IdTaller +
                ", Codigoorden='" + CodigoOrden + '\'' +
                ", IdEtapa=" + IdEtapa +
                ", IdCita=" + IdCita +
                ", IdMecanico=" + IdMecanico +
                ", IdVehiculo=" + IdVehiculo +
                ", kilometraje=" + kilometraje +
                ", DocumentosDeja=" + DocumentosDeja +
                ", Observaciones='" + Observaciones + '\'' +
                ", estado='" + estado + '\'' +
                ", documentos=" + documentos +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

}
