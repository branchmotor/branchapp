package com.co.bog.branch.ws.models;

import java.util.Date;

public class Usuario {

    private Integer IdUsuario;

    private String firstName;

    private String lastName;

    private String identificacion;

    private String email;

    private String uid;

    private String celular;

    private String tipoUsuario;

    private String estado;

    private String tokenCM;

    private String password;

    private Date createdAt;

    private Date updateAt;

    public Usuario() {

    }

    public Usuario(Integer idUsuario, String firstName, String lastName, String identificacion, String email, String uid, String celular, String tipoUsuario, String estado, Date createdAt, Date updateAt) {
        IdUsuario = idUsuario;
        this.firstName = firstName;
        this.lastName = lastName;
        this.identificacion = identificacion;
        this.email = email;
        this.uid = uid;
        this.celular = celular;
        this.tipoUsuario = tipoUsuario;
        this.estado = estado;
        this.createdAt = createdAt;
        this.updateAt = updateAt;
    }

    public Usuario(String email, String uid, String celular) {
        this.email = email;
        this.uid = uid;
        this.celular = celular;
    }

    public Integer getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        IdUsuario = idUsuario;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTokenCM() {
        return tokenCM;
    }

    public void setTokenCM(String tokenCM) {
        this.tokenCM = tokenCM;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "IdUsuario=" + IdUsuario +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", email='" + email + '\'' +
                ", uid='" + uid + '\'' +
                ", celular='" + celular + '\'' +
                ", tipoUsuario='" + tipoUsuario + '\'' +
                ", estado='" + estado + '\'' +
                ", tokenCM='" + tokenCM + '\'' +
                ", createdAt=" + createdAt +
                ", updateAt=" + updateAt +
                '}';
    }
}
