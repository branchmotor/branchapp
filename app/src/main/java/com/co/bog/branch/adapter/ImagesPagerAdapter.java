package com.co.bog.branch.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.co.bog.branch.R;
import com.co.bog.branch.ws.models.Documento;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import static androidx.core.view.ViewCompat.setTransitionName;

public class ImagesPagerAdapter extends PagerAdapter {

    private static final String LOG_TAG = ImagesPagerAdapter.class.getSimpleName();

    private ImageView image;

    private RecyclerView indicatorRecycler;

    private List<Documento> allImages;

    private ProgressBar progressBar;

    public ImagesPagerAdapter(ImageView image, RecyclerView indicatorRecycler, List<Documento> allImages) {
        this.image = image;
        this.indicatorRecycler = indicatorRecycler;
        this.allImages = allImages;
    }

    @Override
    public int getCount() {
        return allImages.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup containerCollection, int position) {
        LayoutInflater layoutinflater = (LayoutInflater) containerCollection.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutinflater.inflate(R.layout.picture_browser_pager,null);
        image = view.findViewById(R.id.image);
        if (view != null) {
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.VISIBLE);
        }

        setTransitionName(image, String.valueOf(position)+"picture");

        Documento documento = allImages.get(position);

        Picasso.get().load(documento.getUrl()).fit()
                .into(image, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d(LOG_TAG, " Se cargo exitosamente la URL");
                        if (progressBar != null) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.d(LOG_TAG, "Error al cargar la imagen ::::> "+ e.getMessage());
                    }
                });


            /*Glide.with(animeContx)
                    .load(pic.getPicturePath())
                    .apply(new RequestOptions().fitCenter())
                    .into(image);*/


        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(indicatorRecycler.getVisibility() == View.GONE){
                    indicatorRecycler.setVisibility(View.VISIBLE);
                }else{
                    indicatorRecycler.setVisibility(View.GONE);
                }

                /**
                 * uncomment the below condition and comment the one above to control recyclerView visibility automatically
                 * when image is clicked
                 */
                    /*if(viewVisibilityController == 0){
                     indicatorRecycler.setVisibility(View.VISIBLE);
                     visibiling();
                 }else{
                     viewVisibilitylooper++;
                 }*/

            }
        });



        ((ViewPager) containerCollection).addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup containerCollection, int position, Object view) {
        ((ViewPager) containerCollection).removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }
}
