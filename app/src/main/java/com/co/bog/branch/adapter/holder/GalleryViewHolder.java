package com.co.bog.branch.adapter.holder;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.DetailsCitaAdapter;
import com.co.bog.branch.adapter.PhotoAdapter;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Orden;
import com.google.android.material.button.MaterialButton;

import java.text.SimpleDateFormat;

import moe.feng.common.stepperview.VerticalStepperItemView;

public class GalleryViewHolder {

    private static final String LOG_TAG = GalleryViewHolder.class.getSimpleName();

    private RecyclerView recyclerViewGallery;

    private RecyclerView.Adapter mAdapter;

    private RecyclerView.LayoutManager mLayoutManager;

    private View view;

    private TextView textViewFecha;

    private TextView textViewMecanico;

    private Orden orden;

    public GalleryViewHolder(View view){

        this.recyclerViewGallery = (RecyclerView) view.findViewById(R.id.galleryRecycler);

        this.textViewFecha = (TextView) view.findViewById(R.id.textFecha);

        this.textViewMecanico = (TextView) view.findViewById(R.id.textMecancio);

        this.view = view;

    }


    public void bind(Cita cita, int  index, VerticalStepperItemView item, final DetailsCitaAdapter.OnItemCitaListener onItemClickListener){

        //Set item to DONE
        if (cita != null && item.getState() != VerticalStepperItemView.STATE_SELECTED){
            item.setState(VerticalStepperItemView.STATE_DONE);
        }

        this.orden = cita.getOrdenes().get(index);

        SimpleDateFormat formatToText = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

        this.textViewFecha.setText(formatToText.format(this.orden.getCreatedAt()));

        this.textViewMecanico.setText(this.orden.getMecanico().getFullName());

        mLayoutManager = new GridLayoutManager(view.getContext(),2,RecyclerView.VERTICAL,false);

        recyclerViewGallery.setLayoutManager(mLayoutManager);
        recyclerViewGallery.setHasFixedSize(true);

        mAdapter = new PhotoAdapter(cita.getOrdenes().get(index).getDocumentos(), R.layout.photo_item, new PhotoAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int position) {

                Log.d(LOG_TAG,"Click en foto de la galeria position "+position);

                onItemClickListener.OnGoToDetailPhoto(cita,index,position);

                /*
                if(idetapa == OrdenEtapa.DIAGNOSTICO.getValue()){
                    switchFragmentlistener.onSwitchDetailDiagnosticoPhotos(listPhotos,position);
                }

                if(idetapa == OrdenEtapa.REPARACION.getValue()){
                    switchFragmentlistener.onSwitchDetailReparacionPhotos(listPhotos,position);
                }*/

            }
        });

        recyclerViewGallery.setAdapter(mAdapter);


        /*this.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.OnNextClick();
            }
        });*/



        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.OnGoToItem(index);
            }
        });

    }



}
