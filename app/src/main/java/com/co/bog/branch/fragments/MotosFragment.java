package com.co.bog.branch.fragments;


import android.Manifest;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.co.bog.branch.R;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.co.bog.branch.adapter.VehiculosViewAdapter;
import com.co.bog.branch.app.CustomProgress;
import com.co.bog.branch.ws.models.Documento;
import com.co.bog.branch.ws.models.Vehiculo;
import com.co.bog.branch.ws.service.BranchService;
import com.co.bog.branch.ws.ws;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class MotosFragment extends Fragment implements FloatingActionButton.OnClickListener {

    private static final String LOG_TAG = MotosFragment.class.getSimpleName();

    private List<Vehiculo> listVehiculos = new ArrayList<>();

    private RecyclerView recyclerViewVehiculos;

    private RecyclerView.Adapter mAdapter;

    private RecyclerView.LayoutManager mLayoutManager;

    private FloatingActionButton floatButtonNewVehicle;

    static SwitchFragmentListener firstPageListener;

    private String currentPhotoPath;

    private Vehiculo Myvehiculo;

    private static final int REQUEST_IMAGE_CAPTURE = 100;

    private static final int REQUEST_IMAGE_GALERY = 200;

    private static final int STORAGE_PERMISSION_CODE = 300;

    private static final int CAMERA_PERMISSION_CODE = 400;

    private FirebaseAuth mAuth;

    private View view;

    public MotosFragment(){
        //Default constructor required
    }


    public MotosFragment(SwitchFragmentListener listener) {
        // Required empty public constructor
        this.firstPageListener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_motos, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        mAuth = FirebaseAuth.getInstance();

        this.recyclerViewVehiculos = (RecyclerView) view.findViewById(R.id.vehiculosRecycler);

        this.floatButtonNewVehicle = (FloatingActionButton) view.findViewById(R.id.floatButtonNewVehicle);

        this.floatButtonNewVehicle.setOnClickListener(this);

        EnableRuntimePermissionToAccessCamera();

        setupRecyclerView(recyclerViewVehiculos);

    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {

        mLayoutManager = new LinearLayoutManager(getActivity());


        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);

        fetchVehiculos();

    }


    private void fetchVehiculos() {

        BranchService service = ws.getService(getContext()).create(BranchService.class);

        FirebaseUser user = mAuth.getCurrentUser();

        Call<List<Vehiculo>> vehiculoCall = service.listVehiculos(user.getUid());

        CustomProgress customProgress = CustomProgress.getInstance();

        customProgress.showProgress(getContext(), R.string.lblEsperar, false);

        vehiculoCall.enqueue(new Callback<List<Vehiculo>>() {
            @Override
            public void onResponse(Call<List<Vehiculo>> call, Response<List<Vehiculo>> response) {
                if (response.isSuccessful()) {
                    listVehiculos = response.body();
                    customProgress.hideProgress();
                    Log.i(LOG_TAG, "Resultado listar vehiculos :::>" + listVehiculos.toString());
                    //mAdapter.notifyDataSetChanged();
                    //mAdapter.notifyAll();
                    mAdapter = new VehiculosViewAdapter(listVehiculos, R.layout.vehiculo_item, new VehiculosViewAdapter.OnItemClickListener() {
                        @Override
                        public void OnItemClick(Vehiculo vehiculo, int position, int layout) {
                            Log.d(LOG_TAG, "Layout button Id :::>" + layout);
                            Log.d(LOG_TAG, "Layout button Id :::>" + R.id.buttonEditarVehiculo);
                            Myvehiculo = vehiculo;
                            switch (layout) {
                                case R.id.buttonEditarVehiculo:
                                    firstPageListener.onSwitchVehicleFragment(vehiculo);
                                    break;
                                case R.id.buttonVerDocumentos:
                                    firstPageListener.onSwitchVehicleDocumentosFragment(vehiculo);
                                    break;
                                case R.id.layoutImageVehiculo:
                                    selectedImage();
                                    break;
                            }


                        }
                    });

                    recyclerViewVehiculos.setAdapter(mAdapter);
                } else {
                    int statusCode = response.code();
                    Log.e(LOG_TAG, "onResponse(): Motos Fragment Error code = " + statusCode);
                    Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<List<Vehiculo>> call, Throwable t) {
                //Toast.makeText(MotosFragment.this,"Error", Toast.LENGTH_SHORT).show();
                Log.e(LOG_TAG, "Error consumir WS = " + t.getMessage());
                Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                        .show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        firstPageListener.onSwitchVehicleFragment(null);
    }

    private void selectedImage() {

        final CharSequence[] options = {getString(R.string.lblTomarPhoto), getString(R.string.lblBuscarPhoto), getString(R.string.lblCancelar)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.lblMuestraPhoto)
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        if (options[which].equals(getString(R.string.lblTomarPhoto))) {

                            dispatchTakePictureIntent();

                        } else if (options[which].equals(getString(R.string.lblBuscarPhoto))) {
                            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, REQUEST_IMAGE_GALERY);
                        } else if (options[which].equals(getString(R.string.lblCancelar))) {
                            dialog.dismiss();

                        }
                    }
                }).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(LOG_TAG, "Activity Result  RequestCode ::> " + requestCode + " ResultCode ::> " + resultCode + " Data ::>" + data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            Log.d(LOG_TAG, "Se va a cargar de la URL " + currentPhotoPath);

            //Uri selectedImageURI = Uri.parse(currentPhotoPath);
            File file = new File(currentPhotoPath);

            // Inicializar el proveedor de credenciales de Amazon Cognito
            uploadToS3(file);

            Log.i(LOG_TAG, "Se cargo la foto, IdVehiculo :::>" + Myvehiculo.getIdVehiculo());


        } else {
            if (requestCode == REQUEST_IMAGE_GALERY && resultCode == RESULT_OK) {

                Uri selectedImage = data.getData();

                String[] filePath = { MediaStore.Images.Media.DATA };

                Log.d(LOG_TAG,"Path image load first File path "+filePath[0]);

                Cursor c = getActivity().getContentResolver().query(selectedImage,filePath, null, null, null);

                c.moveToFirst();

                int columnIndex = c.getColumnIndex(filePath[0]);

                String picturePath = c.getString(columnIndex);

                Log.d(LOG_TAG,"Path image load form Gallery "+picturePath);

                c.close();

                uploadToS3(new File(picturePath));

                /*
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                thumbnail=getResizedBitmap(thumbnail, 400);
                Log.w(LOG_TAG, picturePath+"");
                //IDProf.setImageBitmap(thumbnail);
                BitMapToString(thumbnail);*/
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    /**
     *
     * @param file
     */
    private void uploadToS3(File file) {

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getActivity().getApplicationContext(),
                "us-east-2:2b36dfa2-1fe0-400a-82cb-cd167a14cc3a", // ID del grupo de identidades
                Regions.US_EAST_2 // Región
        );

        Log.d(LOG_TAG," File get name key to s3 :::>"+file.getName());

        AmazonS3 s3 = new AmazonS3Client(credentialsProvider);

        String extension = file.getName().substring(file.getName().lastIndexOf("."));

        String key = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())+extension;

        TransferUtility transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());
        //transferUtility.upload()
        final TransferObserver observer = transferUtility.upload(
                "branchmedia",  //this is the bucket name on S3
                key, //this is the path and name
                //file.getName(),
                file, //path to the file locally
                CannedAccessControlList.PublicRead //to make the file public
        );

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.equals(TransferState.COMPLETED)) {
                    //Success
                    try {

                        Log.d(LOG_TAG,"Se subio el archivo exitosamente to S3 ::>"+state.name());

                        String url = "https://branchmedia.s3.us-east-2.amazonaws.com/"+key;

                        List<Documento> arrayFotos = new ArrayList<Documento>();

                        Documento foto = new Documento();

                        foto.setUrl(url);

                        Calendar now = Calendar.getInstance();

                        foto.setDate(now.getTime().toString());

                        foto.setSize((int) file.length());

                        URLConnection connection = file.toURL().openConnection();

                        String mimeType = connection.getContentType();

                        foto.setType(mimeType);

                        foto.setNombreArchivo(file.getName());

                        foto.setKeynameFile(key);

                        arrayFotos.add(foto);

                        Log.d(LOG_TAG," ArrayFotos Result ::::>"+arrayFotos);

                        BranchService service = ws.getService(getContext()).create(BranchService.class);

                        Myvehiculo.setFotos(arrayFotos);

                        Call<Object> vehiculoCall = service.updateVehiculo(Myvehiculo.getIdVehiculo(), Myvehiculo);

                        vehiculoCall.enqueue(new Callback<Object>() {
                            @Override
                            public void onResponse(Call<Object> call, Response<Object> response) {
                                if (response.isSuccessful()) {
                                    Log.d(LOG_TAG,"Se actualizo correctamente las fotos al vehiculo");
                                    fetchVehiculos();
                                } else {
                                    int statusCode = response.code();
                                    Log.e(LOG_TAG, "onResponse(): Respuesta fallida al actualizar fotos = " + statusCode);
                                }
                            }

                            @Override
                            public void onFailure(Call<Object> call, Throwable t) {
                                //Toast.makeText(MotosFragment.this,"Error", Toast.LENGTH_SHORT).show();
                                Log.e(LOG_TAG, "Error al actualizar las fotos WS = " + t.getMessage());
                                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });

                    }catch (MalformedURLException e){
                        Log.e(LOG_TAG,"Error MalFormed URL "+e.getMessage());
                    }catch (IOException e){
                        Log.e(LOG_TAG,"Error IO File "+e.getMessage());
                    }


                } else if (state.equals(TransferState.FAILED)) {
                    //Failed
                    Log.d(LOG_TAG,"Error al subir archivo to S3");
                }

            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                Log.d(LOG_TAG,"Progress Changed, bytesCurrent ::> "+bytesCurrent+ " Total ::>"+bytesTotal);
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.d(LOG_TAG,"Error en S3 "+ex.getMessage());
            }
        });
    }


    private void dispatchTakePictureIntent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        // Error occurred while creating the File

                    }
                    Log.d(LOG_TAG, "File result :::>" + photoFile.getAbsolutePath());


                    // Continue only if the File was successfully created
                    if (photoFile != null) {

                        Uri photoURI = FileProvider.getUriForFile(getActivity(),
                                "com.mybranch.fileprovider",
                                photoFile);
                        Log.d(LOG_TAG, "Photo URI :::>" + photoURI);

                        //this.vehiculo = vehiculo;
                        //takePictureIntent.putExtra("IdVehiculo", idvehiculo);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            } else {
                Log.d(LOG_TAG, " No tiene permisos los va a solicitar");

                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Log.d(LOG_TAG, " Access fine location request permission before");
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                } else {
                    Log.d(LOG_TAG, " Ya denego los permisos los va a solicitar");
                    // Ha denegado
                    Toast.makeText(getContext(), "Por favor configure los permisos de ubicacion para poder usar este modulo :::> ", Toast.LENGTH_LONG).show();
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                }
            }
        }

    }

    // Requesting runtime permission to access camera.
    public void EnableRuntimePermissionToAccessCamera() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.CAMERA)) {

            // Printing toast message after enabling runtime permission.
            Toast.makeText(getActivity(), "CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);

        }
    }


    private boolean checkPermission(String permission) {
        int result = getActivity().checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        String permission = permissions[0];
        int result = grantResults[0];
        switch (requestCode) {
            case CAMERA_PERMISSION_CODE:

                if (permission.equals(Manifest.permission.CAMERA)) {
                    if (result == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getActivity(), "Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();
                }
                break;
            case STORAGE_PERMISSION_CODE:
                if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Comprobar si ha sido aceptado el permiso
                    if (result == PackageManager.PERMISSION_GRANTED) {
                        // Concedio el permiso
                        Log.d(LOG_TAG, " Se dio el permiso a storage ");

                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

                            // Create the File where the photo should go
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch (IOException ex) {
                                // Error occurred while creating the File

                            }
                            Log.d(LOG_TAG, "File result :::>" + photoFile.getAbsolutePath());


                            // Continue only if the File was successfully created
                            if (photoFile != null) {
                                Uri mPhotoUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
                        /*Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.mybranch.fileprovider",
                        photoFile);*/
                                Log.d(LOG_TAG, "Photo URI :::>" + mPhotoUri);

                                //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                            }
                        }

                    } else {
                        // No concedio el permiso
                        Toast.makeText(getContext(), "No doy permiso", Toast.LENGTH_LONG).show();
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_VEHICULO_PHOTO_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        Log.d(LOG_TAG, " Storage dir ::::>" + storageDir);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        Log.d(LOG_TAG, " Current photo ::> " + currentPhotoPath);
        return image;
    }



    /*public String BitMapToString(Bitmap userImage1) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        userImage1.compress(Bitmap.CompressFormat.PNG, 60, baos);
        byte[] b = baos.toByteArray();
        Document_img1 = Base64.encodeToString(b, Base64.DEFAULT);
        return Document_img1;
    }*/

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


}
