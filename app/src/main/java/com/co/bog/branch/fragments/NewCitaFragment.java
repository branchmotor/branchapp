package com.co.bog.branch.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.co.bog.branch.util.Validacion;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Vehiculo;
import com.co.bog.branch.ws.service.BranchService;
import com.co.bog.branch.ws.ws;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewCitaFragment extends Fragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, View.OnClickListener {

    private static final String LOG_TAG = NewCitaFragment.class.getSimpleName();

    static SwitchFragmentListener switchFragmentCita;

    private TextInputEditText textInputFechaCita;

    private AutoCompleteTextView autoCompleteVehicle;

    private TextInputEditText textInputHoraCita;

    private AutoCompleteTextView autoCompleteService;

    private MaterialButton btnCreateCita;

    private DatePickerDialog dpd;

    private TimePickerDialog tpd;

    private FirebaseAuth mAuth;

    private Cita cita;

    private View view;


    public NewCitaFragment(SwitchFragmentListener listener) {
        // Required empty public constructor
        switchFragmentCita = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_new_cita, container, false);
        bindUI(view);
        return view;
    }

    public void bindUI(View view) {

        mAuth = FirebaseAuth.getInstance();
        this.textInputFechaCita = (TextInputEditText) view.findViewById(R.id.txtCreateFechaCita);
        this.autoCompleteVehicle = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteVehiculo);
        this.autoCompleteService = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteServicio);
        this.textInputHoraCita = (TextInputEditText) view.findViewById(R.id.txtCreateHoraCita);
        this.btnCreateCita = (MaterialButton) view.findViewById(R.id.btnCreateCita);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(), R.array.services_array, R.layout.dropdown_menu_popup_item);
        // Apply the adapter to the spinner
        autoCompleteService.setAdapter(adapter);

        this.textInputFechaCita.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showCalendarPicker();
                } else {
                    Log.d("DatePickerDialog", "Dialog was cancelled");
                    dpd = null;
                }
            }
        });

        this.textInputHoraCita.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showTimePicker();
                } else {
                    Log.d("TimePickerDialog", "Dialog was cancelled");
                    tpd = null;
                }
            }
        });

        this.btnCreateCita.setOnClickListener(this);


        BranchService service = ws.getService(getContext()).create(BranchService.class);

        Call<List<Vehiculo>> vehiculoCall = service.listVehiculos(mAuth.getUid());

        vehiculoCall.enqueue(new Callback<List<Vehiculo>>() {
            @Override
            public void onResponse(Call<List<Vehiculo>> call, Response<List<Vehiculo>> response) {
                if (response.isSuccessful()) {
                    List<Vehiculo> vehiculos = response.body();
                    List<String> textPlacas = new ArrayList<>();
                    for (Vehiculo vehiculo : vehiculos) {
                        textPlacas.add(vehiculo.getPlaca());
                    }

                    Log.d(LOG_TAG, " Listado de marcada " + vehiculos);

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(), R.layout.dropdown_menu_popup_item, textPlacas);

                    autoCompleteVehicle.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Vehiculo>> call, Throwable t) {
                Log.e(LOG_TAG, " Error al listar marcas " + t.getMessage());
                Toast.makeText(getActivity(), "Fallo al list marcas", Toast.LENGTH_LONG).show();
            }
        });


    }

    private void showCalendarPicker() {
        Calendar now = Calendar.getInstance();
        /*
        It is recommended to always create a new instance whenever you need to show a Dialog.
        The sample app is reusing them because it is useful when looking for regressions
        during testing
         */
        if (dpd == null) {
            dpd = DatePickerDialog.newInstance(
                    NewCitaFragment.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
        } else {
            dpd.initialize(
                    NewCitaFragment.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
        }

        dpd.showYearPickerFirst(true);
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);

        //No Citas antes de hoy
        dpd.setMinDate(now);

        dpd.setTitle("Fecha cita");

        dpd.setOnCancelListener(dialog -> {
            Log.d("DatePickerDialog", "Dialog was cancelled");
            dpd = null;
        });
        dpd.show(requireFragmentManager(), "Datepickerdialog");
    }

    private void showTimePicker() {
        Calendar now = Calendar.getInstance();
        /*
        It is recommended to always create a new instance whenever you need to show a Dialog.
        The sample app is reusing them because it is useful when looking for regressions
        during testing
         */
        if (tpd == null) {
            tpd = TimePickerDialog.newInstance(
                    NewCitaFragment.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE), false);
        } else {
            tpd = TimePickerDialog.newInstance(
                    NewCitaFragment.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE), false);
        }

        tpd.setTimeInterval(1, 5, 60);

        tpd.setVersion(TimePickerDialog.Version.VERSION_2);

        tpd.setTitle("Hora cita");

        tpd.setOnCancelListener(dialog -> {
            Log.d("TimePickerDialog", "Dialog was cancelled");
            tpd = null;
        });
        tpd.show(requireFragmentManager(), "Timepickerdialog");
    }

    public void backPressed() {
        switchFragmentCita.onSwitchCitaFragment();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        Log.d(LOG_TAG, "Dia seleccinado :::> " + date);
        this.textInputFechaCita.setText(date);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        //Log.d(LOG_TAG, "Dato de hora selected :::>" + hourOfDay + " Minute ::.>" + minute);
        String hourdayText = hourOfDay <= 12 ? (hourOfDay == 0) ? (hourOfDay + 12) + "" : hourOfDay + "" : (hourOfDay - 12) + "";
        String ampm = hourOfDay < 12 ? "AM" : "PM";
        String time = hourdayText + ":" + String.format("%02d", minute) + " " + ampm;
        this.textInputHoraCita.setText(time);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCreateCita:
                createCita();
        }
    }

    private void createCita() {
        if (checkValidation()) {

            BranchService service = ws.getService(getContext()).create(BranchService.class);

            cita = new Cita();
            cita.setServicio(this.autoCompleteService.getText().toString());
            cita.setEstado("Solicitada");
            cita.setPlaca(this.autoCompleteVehicle.getText().toString());

            try {

                SimpleDateFormat formatHour = new SimpleDateFormat("h:mm a",Locale.FRANCE);

                Date date = formatHour.parse(this.textInputHoraCita.getText().toString());

                SimpleDateFormat format24 = new SimpleDateFormat("H:mm");

                this.cita.setHoraCita(format24.format(date));

                this.cita.setFechaCita(this.textInputFechaCita.getText().toString());


                Call<Vehiculo> vehiculoCall = service.getVehiculoByPlaca(this.cita.getPlaca());


                vehiculoCall.enqueue(new Callback<Vehiculo>() {
                    @Override
                    public void onResponse(Call<Vehiculo> call, Response<Vehiculo> response) {
                        if (response.isSuccessful()) {
                            Vehiculo vehiculo = response.body();
                            if (vehiculo.getIdTaller() != null) {

                                cita.setTaller(vehiculo.getIdTaller());

                                Call<Cita> citaCall = service.crearCita(cita);

                                citaCall.enqueue(new Callback<Cita>() {
                                    @Override
                                    public void onResponse(Call<Cita> call, Response<Cita> response) {
                                        if (response.isSuccessful()) {
                                            Snackbar.make(view, R.string.msgSuccessCreateCita, Snackbar.LENGTH_LONG)
                                                    .show();
                                            switchFragmentCita.onSwitchCitaFragment();
                                        } else {
                                            int statusCode = response.code();
                                            Log.e(LOG_TAG, "onResponse(): Codigo respuesta crear Cita = " + statusCode + " Message :::> " + response.message());
                                            Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                                                    .show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Cita> call, Throwable t) {
                                        Log.e(LOG_TAG, " Error al crear cita " + t.getMessage());
                                        Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                                                .show();
                                    }
                                });
                            } else {
                                Snackbar.make(view, R.string.msgVehiculoSinTaller, Snackbar.LENGTH_LONG)
                                        .show();

                            }
                        } else {
                            int statusCode = response.code();
                            Log.e(LOG_TAG, "onResponse(): Codigo respuesta al buscar vehiculo por placa = " + statusCode + " Message :::> " + response.message());
                            Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Vehiculo> call, Throwable t) {
                        Log.e(LOG_TAG, " Fallo al obtener vehiculo por placa " + t.getMessage());
                        Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                                .show();
                    }
                });


            } catch (ParseException e) {
                Log.e(LOG_TAG, "Error al parsear la hora a formato 24 horas ::> "+ e.getMessage());
                Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                        .show();
            }
        } else {
            Snackbar.make(view, R.string.msgFormFailed, Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    private boolean checkValidation() {
        boolean ret = true;

        if (!Validacion.hasText(this.textInputFechaCita)) ret = false;
        if (!Validacion.hasText(this.textInputHoraCita)) ret = false;
        if (!Validacion.hasText(this.autoCompleteService)) ret = false;
        if (!Validacion.hasText(this.autoCompleteVehicle)) ret = false;

        return ret;
    }
}
