package com.co.bog.branch.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.co.bog.branch.R;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Documento;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import static androidx.core.view.ViewCompat.setTransitionName;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder>{

    private static final String LOG_TAG = PhotoAdapter.class.getSimpleName();

    private List<Documento> listPhotos;

    private OnItemClickListener itemClickListener;

    private int layout;

    public PhotoAdapter(List<Documento> listPhotos, int layout, OnItemClickListener itemClickListener) {
        this.listPhotos = listPhotos;
        this.layout = layout;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        PhotoViewHolder vh = new PhotoViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder holder, int position) {
        holder.bind(this.listPhotos.get(position), this.listPhotos ,itemClickListener);
    }

    @Override
    public int getItemCount() {
        return this.listPhotos.size();
    }

    public static class PhotoViewHolder extends RecyclerView.ViewHolder {

        private ImageView photo;

        private ProgressBar progressBar;


        public PhotoViewHolder(@NonNull View itemView){
            super(itemView);
            this.photo = (ImageView) itemView.findViewById(R.id.imageThumbnail);
            if (itemView != null) {
                progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
                progressBar.setVisibility(View.VISIBLE);
            }
        }

        public void bind(final Documento documento, List<Documento> listDocumentos,  final OnItemClickListener onItemClickListener) {

            Log.d(LOG_TAG," Se va a cargar la ULR del documento ::::>" + documento.getUrlThumbnail());
            Picasso.get().load(documento.getUrlThumbnail())
                    .into(this.photo, new Callback() {
                        @Override
                        public void onSuccess() {
                            Log.d(LOG_TAG, " Se cargo exitosamente la URL");
                            if (progressBar != null) {
                                progressBar.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.d(LOG_TAG, "Error al cargar la imagen ::::> "+ e.getMessage());
                        }
                    });

            setTransitionName(this.photo, String.valueOf(getAdapterPosition()) + "_image");

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClick(getAdapterPosition());
                }
            });
        }
    }

    public interface OnItemClickListener {
        void OnItemClick( int position);
    }
}
