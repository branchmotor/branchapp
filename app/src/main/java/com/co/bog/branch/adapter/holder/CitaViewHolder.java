package com.co.bog.branch.adapter.holder;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.listener.OnItemClickListener;
import com.co.bog.branch.ws.models.Cita;
import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CitaViewHolder extends RecyclerView.ViewHolder {

    private static final String LOG_TAG= CitaViewHolder.class.getSimpleName();

    private TextView textNombreServicio;

    private TextView textNombreTaller;

    private TextView textPlacaVehiculo;

    private TextView textEstadoCita;

    private TextView textFechaCita;

    private View itemView;


    public CitaViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        this.textNombreServicio = (TextView) itemView.findViewById(R.id.textNombreServicio);
        this.textNombreTaller = (TextView) itemView.findViewById(R.id.textNombreTaller);
        this.textPlacaVehiculo = (TextView) itemView.findViewById(R.id.textPlacaVehiculo);
        this.textEstadoCita = (TextView) itemView.findViewById(R.id.textEstadoCita);
        this.textFechaCita = (TextView) itemView.findViewById(R.id.textFechaCita);
    }

    public void bind(final Cita cita, final OnItemClickListener onItemClickListener) {

        this.textNombreServicio.setText(cita.getServicio());
        this.textNombreTaller.setText(cita.getObjTaller().getNombre());
        this.textPlacaVehiculo.setText(cita.getVehiculo().getPlaca());
        this.textEstadoCita.setText(cita.getEstado());

        try {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            Date dateFormat = format.parse(cita.getFechaCita());

            SimpleDateFormat formatText = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
            this.textFechaCita.setText(formatText.format(dateFormat));

        }catch (ParseException e){
            Log.e(LOG_TAG, "Error al parsear la fecha");
        }


        this.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG,"Click en la vista "+v.getId());
                if(cita.getOrdenes().size() > 0){
                    onItemClickListener.OnItemClick(cita, getAdapterPosition(),v.getId());
                }else{
                    Snackbar.make(v, R.string.msgCitaSinOrdenes, Snackbar.LENGTH_LONG)
                            .show();
                }
            }
        });
    }


}
