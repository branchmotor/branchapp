package com.co.bog.branch.ws.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Cita {

    private Integer IdCita;

    private Integer taller;

    private Integer IdMecanico;

    private Integer IdVehiculo;

    private String fechaCita;

    private String horaCita;

    private Date createdAt;

    private Date updatedAt;

    private String estado;

    private String servicio;

    private String placa;

    private Vehiculo vehiculo;



    private Taller objTaller;

    private List<Orden> ordenes = new ArrayList<Orden>();



    public Cita(){

    }



    public Integer getIdCita() {
        return IdCita;
    }

    public void setIdCita(Integer idCita) {
        IdCita = idCita;
    }

    public Integer getTaller() {
        return taller;
    }

    public void setTaller(Integer taller) {
        this.taller = taller;
    }

    public Integer getIdMecanico() {
        return IdMecanico;
    }

    public void setIdMecanico(Integer idMecanico) {
        IdMecanico = idMecanico;
    }

    public Integer getIdVehiculo() {
        return IdVehiculo;
    }

    public void setIdVehiculo(Integer idVehiculo) {
        IdVehiculo = idVehiculo;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getHoraCita() {
        return horaCita;
    }

    public void setHoraCita(String horaCita) {
        this.horaCita = horaCita;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Taller getObjTaller() {
        return objTaller;
    }

    public void setObjTaller(Taller objTaller) {
        this.objTaller = objTaller;
    }

    public List<Orden> getOrdenes() {
        return ordenes;
    }

    public void setOrdenes(List<Orden> ordenes) {
        this.ordenes = ordenes;
    }

    public void addOrden(Orden orden){
        this.ordenes.add(orden);
    }


    @Override
    public String toString() {
        return "Cita{" +
                "IdCita=" + IdCita +
                ", taller=" + taller +
                ", IdMecanico=" + IdMecanico +
                ", IdVehiculo=" + IdVehiculo +
                ", fechaCita=" + fechaCita +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", vehiculo=" + vehiculo +
                '}';
    }
}
