package com.co.bog.branch.util;

import android.content.SharedPreferences;

public class Util {

    public static String getUserEmailPrefs(SharedPreferences preferences){
        return preferences.getString("email", "");
    }

    public static String getUserPassPrefs(SharedPreferences preferences){
        return preferences.getString("password","");
    }

    public static String generateThumbnailUrl(String url){
        String myurl = url.replaceAll("branchmedia","branchmedia-resized");
        return myurl;
    }

    public static String getKeyFileFromURL(String url){
        String mykey = url.substring(url.lastIndexOf("/")+1, url.length());
        return mykey;
    }

}
