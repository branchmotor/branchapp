package com.co.bog.branch.ws.models;

public class Mecanico {

    private Integer IdMecanico;

    private String identificacion;

    private String fullName;

    public Mecanico(){

    }

    public Integer getIdMecanico() {
        return IdMecanico;
    }

    public void setIdMecanico(Integer idMecanico) {
        IdMecanico = idMecanico;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "Mecanico{" +
                "IdMecanico=" + IdMecanico +
                ", identificacion='" + identificacion + '\'' +
                ", fullName='" + fullName + '\'' +
                '}';
    }
}
