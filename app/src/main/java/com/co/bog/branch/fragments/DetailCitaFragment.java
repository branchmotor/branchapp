package com.co.bog.branch.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.DetailsCitaAdapter;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Orden;
import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import moe.feng.common.stepperview.VerticalStepperView;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailCitaFragment extends Fragment {

    private static final String LOG_TAG= DetailCitaFragment.class.getSimpleName();

    private VerticalStepperView mVerticalStepperView;

    private TextView textNombreServicio;

    private TextView textNombreTaller;

    private TextView textPlacaVehiculo;

    private TextView textEstadoCita;

    private TextView textFechaCita;

    private Cita cita;

    private SwitchFragmentListener switchFragmentListener;


    public DetailCitaFragment() {
        // Required empty public constructor
    }


    public DetailCitaFragment(Cita cita, SwitchFragmentListener switchFragmentListener){
        this.cita = cita;
        this.switchFragmentListener = switchFragmentListener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_detail_cita, container, false);
        bindUI(view);
        return view;
    }

    private void bindUI(View view){
        this.textNombreServicio = (TextView) view.findViewById(R.id.textNombreServicio);
        this.textNombreTaller = (TextView) view.findViewById(R.id.textNombreTaller);
        this.textPlacaVehiculo = (TextView) view.findViewById(R.id.textPlacaVehiculo);
        this.textEstadoCita = (TextView) view.findViewById(R.id.textEstadoCita);
        this.textFechaCita = (TextView) view.findViewById(R.id.textFechaCita);

        if(cita != null){
            this.textNombreServicio.setText(cita.getServicio());
            this.textNombreTaller.setText(cita.getObjTaller().getNombre());
            this.textPlacaVehiculo.setText(cita.getVehiculo().getPlaca());
            this.textEstadoCita.setText(cita.getEstado());

            try {

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                Date dateFormat = format.parse(cita.getFechaCita());

                SimpleDateFormat formatText = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
                this.textFechaCita.setText(formatText.format(dateFormat));

            }catch (ParseException e){
                Log.e(LOG_TAG, "Error al parsear la fecha");
            }

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mVerticalStepperView = (VerticalStepperView) view.findViewById(R.id.vertical_stepper_view);


        mVerticalStepperView.setStepperAdapter(new DetailsCitaAdapter(this.cita,this.cita.getOrdenes(),new DetailsCitaAdapter.OnItemCitaListener() {
            @Override
            public void OnNextClick() {
                if (!mVerticalStepperView.nextStep()) {
                    mVerticalStepperView.setErrorText(0, mVerticalStepperView.getErrorText(0) == null ? "Test error" : null);
                    Snackbar.make(mVerticalStepperView, "Set!", Snackbar.LENGTH_LONG).show();
                }

            }

            @Override
            public void OnPrevClick() {
                if(mVerticalStepperView.canPrev()){
                    mVerticalStepperView.prevStep();
                }
            }

            @Override
            public void OnGoToItem(int index) {
                Log.d(LOG_TAG,"Go to Index :::>" + index);
                if(mVerticalStepperView.getCurrentStep() == index){
                    mVerticalStepperView.setCurrentStep(-1);
                }else{
                    mVerticalStepperView.setCurrentStep(index);
                }

            }


            @Override
            public void viewDetailCotizacion(Cita cita, int indexEtapaPdf) {
                switchFragmentListener.onSwitchVisualizadorPDF(cita);
            }

            @Override
            public void OnGoToDetailPhoto(Cita cita, int indexEtapaPdf , int position) {
                switchFragmentListener.onSwitchDetailPhotos(cita,indexEtapaPdf,position);
            }
        }));

        //Ningun Step selected
        mVerticalStepperView.setCurrentStep(-1);

    }

    public void backPressed() {
        switchFragmentListener.onSwitchDetailCita(cita);
    }
}
