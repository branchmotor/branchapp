package com.co.bog.branch.ws.models;

import java.util.Date;

public class Marca {

    private Integer IdMarca;

    private String marca;

    private String referencia;

    private String categoria;

    private String tipo;

    private String descripcion;

    private String tipoVehiculo;

    private Date createdAt;

    private Date updateAt;

    private String urllogo;

    public Marca() {
    }

    public Marca(Integer idMarca, String marca, String referencia, String categoria, String tipo, String descripcion, String tipoVehiculo, Date createdAt, Date updateAt) {
        IdMarca = idMarca;
        this.marca = marca;
        this.referencia = referencia;
        this.categoria = categoria;
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.tipoVehiculo = tipoVehiculo;
        this.createdAt = createdAt;
        this.updateAt = updateAt;
    }

    public Integer getIdMarca() {
        return IdMarca;
    }

    public void setIdMarca(Integer idMarca) {
        IdMarca = idMarca;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getUrllogo() {
        return urllogo;
    }

    public void setUrllogo(String urllogo) {
        this.urllogo = urllogo;
    }

    @Override
    public String toString() {
        return "Marca{" +
                "IdMarca=" + IdMarca +
                ", marca='" + marca + '\'' +
                ", referencia='" + referencia + '\'' +
                '}';
    }
}
