package com.co.bog.branch.adapter;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.co.bog.branch.R;
import com.co.bog.branch.ws.models.Vehiculo;
import com.google.android.material.button.MaterialButton;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.List;

// import de.hdodenhof.circleimageview.CircleImageView;

public class VehiculosViewAdapter extends RecyclerView.Adapter<VehiculosViewAdapter.ViewHolder> {

    private static final String LOG_TAG = VehiculosViewAdapter.class.getSimpleName();

    private List<Vehiculo> listVehiculos;

    private int layout;

    private OnItemClickListener itemClickListener;


    public VehiculosViewAdapter(List<Vehiculo> listVehiculos, int layout, OnItemClickListener itemClickListener) {
        this.listVehiculos = listVehiculos;
        this.layout = layout;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(this.listVehiculos.get(position), itemClickListener);
    }

    @Override
    public int getItemCount() {
        return this.listVehiculos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView logoMarcaImage;

        private TextView textViewPlaca;

        private TextView textViewNombre;

        private TextView textViewCilindraje;

        private TextView textViewKilometraje;

        private TextView textViewMarcaVehiculo;

        private MaterialButton buttonEditarVehiculo;

        private MaterialButton buttonEliminarVehiculo;

        private MaterialButton buttonVerDocumentos;

        //private CarouselView carouselView;
        private ImageView photoVehiculo;

        int[] sampleImages = {R.drawable.bg_load_photo, R.drawable.bg_sin_logo, R.drawable.ic_logo};

        private ProgressBar progressBar;

        private FrameLayout layoutImageVehiculo;

        public ViewHolder(View itemView) {
            super(itemView);
            this.logoMarcaImage = (ImageView) itemView.findViewById(R.id.logoMarcaImage);
            this.textViewPlaca = (TextView) itemView.findViewById(R.id.textPlacaVehiculo);
            this.textViewNombre = (TextView) itemView.findViewById(R.id.textNombreVehiculo);
            this.textViewCilindraje = (TextView) itemView.findViewById(R.id.textCilindrajeVehiculo);
            this.textViewKilometraje = (TextView) itemView.findViewById(R.id.textKilometrajeVehiculo);
            this.textViewMarcaVehiculo = (TextView) itemView.findViewById(R.id.textMarcaVehiculo);
            this.buttonEditarVehiculo = (MaterialButton) itemView.findViewById(R.id.buttonEditarVehiculo);
            this.buttonVerDocumentos = (MaterialButton) itemView.findViewById(R.id.buttonVerDocumentos);
            this.photoVehiculo = (ImageView) itemView.findViewById(R.id.photoVehiculo);
            this.layoutImageVehiculo = (FrameLayout) itemView.findViewById(R.id.layoutImageVehiculo);
            //this.carouselView = (CarouselView) itemView.findViewById(R.id.carouselView);
            //this.progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);

            if (itemView != null) {
                progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
                progressBar.setVisibility(View.VISIBLE);
            }
        }

        public void bind(final Vehiculo vehiculo, final OnItemClickListener onItemClickListener) {

            Log.i(LOG_TAG, "Se va a reenderizar el vehiculo :::>" + vehiculo+ " Longitud Fotos "+vehiculo.getFotos().size());
            this.textViewMarcaVehiculo.setText(vehiculo.getMarca() == null ? "Sin marca" : vehiculo.getMarca().getMarca());
            this.textViewPlaca.setText(vehiculo.getPlaca());
            this.textViewNombre.setText(vehiculo.getAlias());
            this.textViewCilindraje.setText(vehiculo.getTipoVehiculo());
            this.textViewKilometraje.setText(vehiculo.getKilometraje() == null ? "0" : vehiculo.getKilometraje().toString() + " KM");

            if(vehiculo.getFotos().size() == 0){
                Picasso.get().load(R.drawable.background)
                        .fit()
                        .into(this.photoVehiculo);
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                }
            }else{
                String url = vehiculo.getFotos().get(0).getUrl();

                Picasso.get().load(url)
                        .fit()
                        .into(this.photoVehiculo, new Callback() {
                            @Override
                            public void onSuccess() {
                                Log.d(LOG_TAG, " Se cargo exitosamente la URL");
                                if (progressBar != null) {
                                    progressBar.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onError(Exception e) {
                                Log.d(LOG_TAG, "Error al cargar la imagen ::::> "+ e.getMessage());
                            }
                        });
            }



            /*if(vehiculo.getFotos().size() == 0){
                this.carouselView.setPageCount(sampleImages.length);
            }else{
                this.carouselView.setPageCount(vehiculo.getFotos().size());
            }*/


            /*
            this.carouselView.setImageListener(new ImageListener() {
                @Override
                public void setImageForPosition(int position, ImageView imageView) {
                    if(vehiculo.getFotos().size() == 0){
                        imageView.setImageResource(sampleImages[position]);
                        if (progressBar != null) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }else{
                        String url = vehiculo.getFotos().get(position).getUrl();
                        Log.d(LOG_TAG, "Url foto a cargar " + url);
                        //Uri uri = Uri.parse(url);
                        //imageView.setImageURI(uri);
                        Picasso.get().load(url)
                                .into(imageView, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        Log.d(LOG_TAG, " Se cargo exitosamente la URL");
                                        if (progressBar != null) {
                                            progressBar.setVisibility(View.GONE);
                                        }
                                    }

                                    @Override
                                    public void onError(Exception e) {
                                        Log.d(LOG_TAG, "Error al cargar la imagen ::::> "+ e.getMessage());
                                    }
                                });
                    }


                }
            });*/


            if (vehiculo.getMarca() != null) {
                Log.d(LOG_TAG, "Voy a cargar la imagen de la siguiente URL " + vehiculo.getMarca().getUrllogo());
                Picasso.get().load("https://about.canva.com/wp-content/uploads/sites/3/2016/08/logos-1.png")
                        .into(this.logoMarcaImage);
            }


            this.buttonEditarVehiculo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClick(vehiculo, getAdapterPosition(), v.getId());
                }
            });

            this.buttonVerDocumentos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClick(vehiculo, getAdapterPosition(), v.getId());
                }
            });

            this.layoutImageVehiculo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClick(vehiculo, getAdapterPosition(), v.getId());
                }
            });
        }

    }

    public interface OnItemClickListener {
        void OnItemClick(Vehiculo vehiculo, int position, int element);
    }


}
