package com.co.bog.branch.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.co.bog.branch.R;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.co.bog.branch.app.CustomProgress;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Orden;
import com.co.bog.branch.ws.service.BranchService;
import com.co.bog.branch.ws.ws;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.google.android.material.button.MaterialButton;
import com.shockwave.pdfium.PdfDocument;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CotizacionFragment extends Fragment implements OnPageChangeListener, OnLoadCompleteListener, OnPageErrorListener , View.OnClickListener {

    private static final String LOG_TAG = CotizacionFragment.class.getSimpleName();

    private PDFView pdfView;

    private MaterialButton buttonRechazar;

    private MaterialButton buttonAceptar;

    private Integer pageNumber = 0;

    private Orden orden;

    private Cita cita;

    private SwitchFragmentListener switchFragmentListener;

    private String currentPdfPath;

    private static final int STORAGE_PERMISSION_CODE = 300;

    public CotizacionFragment(Cita cita, SwitchFragmentListener switchFragmentListener) {
        // Required empty public constructor
        this.cita = cita;
        this.orden = cita.getOrdenes().get(2);
        this.switchFragmentListener = switchFragmentListener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cotizacion, container, false);

        bindUI(view);

        return view;
    }

    private void bindUI(@NotNull View view) {

        Log.d(LOG_TAG, "La url a cargar es :::::> "+this.orden.getDocumentos().get(0).getUrl());

        this.pdfView = (PDFView) view.findViewById(R.id.pdfView);

        this.buttonRechazar = (MaterialButton) view.findViewById(R.id.buttonRechazarCotizacion);

        this.buttonAceptar = (MaterialButton) view.findViewById(R.id.buttonAceptarCotizacion);

        if(!this.orden.getEstado().equals("Pendiente")){
            this.buttonAceptar.setVisibility(View.INVISIBLE);
            this.buttonRechazar.setVisibility(View.INVISIBLE);
        }

        //this.buttonAceptar.setOnClickListener(this);

        setFile();

        /**/
    }

    private void setFile(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {

                try {
                    CustomProgress customProgress = CustomProgress.getInstance();

                    customProgress.showProgress(getContext(), R.string.lblEsperar, false);

                    final File pdfFile  = createPdfFile();

                    Log.d(LOG_TAG, "File result :::>" + pdfFile.getAbsolutePath());

                    CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                            getActivity().getApplicationContext(),
                            "us-east-2:2b36dfa2-1fe0-400a-82cb-cd167a14cc3a", // ID del grupo de identidades
                            Regions.US_EAST_2 // Región
                    );

                    AmazonS3 s3 = new AmazonS3Client(credentialsProvider);

                    TransferUtility transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());
                    final TransferObserver observer = transferUtility.download(
                            "branchmedia",  //this is the bucket name on S3
                            this.orden.getDocumentos().get(0).getKeynameFile(), //this is the path and name
                            pdfFile
                    );

                    observer.setTransferListener(new TransferListener() {
                        @Override
                        public void onStateChanged(int id, TransferState state) {
                            if (state.equals(TransferState.COMPLETED)) {
                                customProgress.hideProgress();

                                pdfView.fromFile(pdfFile)
                                        .swipeHorizontal(true)
                                        .pageSnap(true)
                                        .pageFling(true)
                                        .fitEachPage(true)
                                        .enableAntialiasing(true)
                                        .defaultPage(pageNumber)
                                        .onLoad(CotizacionFragment.this)
                                        .onPageChange(CotizacionFragment.this)
                                        .onPageError(CotizacionFragment.this)
                                        .load();
                            }
                        }

                        @Override
                        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                            Log.d(LOG_TAG,"Progress Changed, bytesCurrent ::> "+bytesCurrent+ " Total ::>"+bytesTotal);
                        }

                        @Override
                        public void onError(int id, Exception ex) {

                        }
                    });

                } catch (IOException ex) {
                    // Error occurred while creating the File

                };

            } else {
                Log.d(LOG_TAG, " No tiene permisos los va a solicitar");

                if (!shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Log.d(LOG_TAG, " Access fine location request permission before");
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                } else {
                    Log.d(LOG_TAG, " Ya denego los permisos los va a solicitar");
                    // Ha denegado
                    Toast.makeText(getContext(), "Por favor configure los permisos de ubicacion para poder usar este modulo :::> ", Toast.LENGTH_LONG).show();
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                }
            }
        }

    }

    private File createPdfFile() throws IOException {
        // Create an image file name

        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        Log.d(LOG_TAG, " Storage dir ::::>" + storageDir);
        File archivo = File.createTempFile(
                orden.getDocumentos().get(0).getKeynameFile(),  /* prefix */
                ".pdf",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPdfPath = archivo.getAbsolutePath();
        Log.d(LOG_TAG, " Current photo ::> " + currentPdfPath);
        return archivo;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        String permission = permissions[0];
        int result = grantResults[0];
        switch (requestCode) {
            case STORAGE_PERMISSION_CODE:
                if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    if (result == PackageManager.PERMISSION_GRANTED) {
                        // Concedio el permiso
                        Log.d(LOG_TAG, " Se dio el permiso a storage ");

                        // Create the File where the photo should go

                        try {
                            final File pdfFile = createPdfFile();

                            Log.d(LOG_TAG, "File result :::>" + pdfFile.getAbsolutePath());

                            CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                                    getActivity().getApplicationContext(),
                                    "us-east-2:2b36dfa2-1fe0-400a-82cb-cd167a14cc3a", // ID del grupo de identidades
                                    Regions.US_EAST_2 // Región
                            );

                            AmazonS3 s3 = new AmazonS3Client(credentialsProvider);

                            TransferUtility transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());
                            final TransferObserver observer = transferUtility.download(
                                    "branchmedia",  //this is the bucket name on S3
                                    "file.getName()", //this is the path and name
                                    pdfFile
                            );

                            observer.setTransferListener(new TransferListener() {
                                @Override
                                public void onStateChanged(int id, TransferState state) {
                                    if (state.equals(TransferState.COMPLETED)) {
                                        pdfView.fromFile(pdfFile)
                                                .defaultPage(pageNumber)
                                                .onPageChange(CotizacionFragment.this)
                                                .enableAnnotationRendering(true)
                                                .onLoad(CotizacionFragment.this)
                                                .scrollHandle(new DefaultScrollHandle(getContext()))
                                                .spacing(10) // in dp
                                                .onPageError(CotizacionFragment.this)
                                                .load();
                                    }
                                }

                                @Override
                                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                                    Log.d(LOG_TAG,"Progress Changed, bytesCurrent ::> "+bytesCurrent+ " Total ::>"+bytesTotal);
                                }

                                @Override
                                public void onError(int id, Exception ex) {

                                }
                            });
                        } catch (IOException ex) {
                            // Error occurred while creating the File

                        }

                    }else{
                        Toast.makeText(getContext(), "No doy permiso", Toast.LENGTH_LONG).show();
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                    }
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private boolean checkPermission(String permission) {
        int result = getActivity().checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }



    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        //setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        Log.e(LOG_TAG, "title = " + meta.getTitle());
        Log.e(LOG_TAG, "author = " + meta.getAuthor());
        Log.e(LOG_TAG, "subject = " + meta.getSubject());
        Log.e(LOG_TAG, "keywords = " + meta.getKeywords());
        Log.e(LOG_TAG, "creator = " + meta.getCreator());
        Log.e(LOG_TAG, "producer = " + meta.getProducer());
        Log.e(LOG_TAG, "creationDate = " + meta.getCreationDate());
        Log.e(LOG_TAG, "modDate = " + meta.getModDate());

        printBookmarksTree(pdfView.getTableOfContents(), "-");
    }

    /**
     *
     * @param tree
     * @param sep
     */
    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(LOG_TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    @Override
    public void onPageError(int page, Throwable t) {
        Log.e(LOG_TAG, "Cannot load page " + page);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.buttonAceptarCotizacion:

                BranchService service = ws.getService(getContext()).create(BranchService.class);

                orden.setEstado("Aceptado");

                Call<Object> ordenCall = service.updateOrden(orden.getIdOrdenTrabajo(),orden);

                ordenCall.enqueue(new Callback<Object>() {

                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        if (response.isSuccessful()) {

                            Orden ordenAprobacion = new Orden();
                            ordenAprobacion.setIdTaller(orden.getIdTaller());
                            ordenAprobacion.setCodigoOrden(orden.getCodigoOrden());
                            ordenAprobacion.setIdEtapa(5);
                            ordenAprobacion.setIdCita(orden.getIdCita());
                            ordenAprobacion.setIdMecanico(orden.getIdMecanico());
                            ordenAprobacion.setIdVehiculo(orden.getIdVehiculo());
                            ordenAprobacion.setObservaciones("El cliente aprueba la cotizacion desde la App");
                            ordenAprobacion.setEstado("Aceptado");

                            Call<Orden> ordenCreate = service.crearOrden(ordenAprobacion);

                            ordenCreate.enqueue(new Callback<Orden>() {
                                @Override
                                public void onResponse(Call<Orden> call, Response<Orden> response) {
                                    if (response.isSuccessful()) {
                                        switchFragmentListener.onSwitchVisualizadorPDF(cita);
                                    }else{
                                        int statusCode = response.code();
                                        Log.e(LOG_TAG, "onResponse(): Codigo respuesta Crear orden Error code = " + statusCode);

                                    }
                                }

                                @Override
                                public void onFailure(Call<Orden> call, Throwable t) {
                                    switchFragmentListener.onSwitchVisualizadorPDF(cita);
                                    if (call.isCanceled()) {
                                        System.out.println("Call was cancelled forcefully");
                                        Log.e(LOG_TAG, "Call was canceled WS = " + t.getMessage());
                                    } else {
                                        Log.e(LOG_TAG, "Call Network Error = " + t.getLocalizedMessage());
                                    }
                                }
                            });




                        }else{
                            int statusCode = response.code();
                            Log.e(LOG_TAG, "onResponse(): Codigo respuesta Actualizar orden Error code = " + statusCode);
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        if (call.isCanceled()) {
                            System.out.println("Call was cancelled forcefully");
                            Log.e(LOG_TAG, "Call was canceled WS = " + t.getMessage());
                        } else {
                            Log.e(LOG_TAG, "Call Network Error = " + t.getLocalizedMessage());
                        }
                    }
                });
                break;

        }


    }

    public void backPressed() {
        switchFragmentListener.onSwitchVisualizadorPDF(cita);
    }
}
