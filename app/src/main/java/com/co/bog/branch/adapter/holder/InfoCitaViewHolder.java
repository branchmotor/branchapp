package com.co.bog.branch.adapter.holder;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.DetailsCitaAdapter;
import com.co.bog.branch.ws.models.Orden;

import java.text.SimpleDateFormat;

import moe.feng.common.stepperview.VerticalStepperItemView;

public class InfoCitaViewHolder {

    private static final String LOG_TAG= InfoCitaViewHolder.class.getSimpleName();

    private TextView txtFechaIngreso;

    private TextView txtMecancio;

    private TextView txtObservacion;

    private View view;

    public InfoCitaViewHolder(View view){
        Log.d(LOG_TAG,"View Ingreso ID:::> "+view.getId());
        this.txtFechaIngreso = (TextView) view.findViewById(R.id.textFechaIngreso);
        this.txtObservacion = (TextView) view.findViewById(R.id.textObservacion);
        this.txtMecancio = (TextView) view.findViewById(R.id.textMecancio);
        this.view = view;
    }

    public void bind(Orden orden, int  index, VerticalStepperItemView item, final DetailsCitaAdapter.OnItemCitaListener onItemClickListener){


        /*SimpleDateFormat formatToDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date formatDate = formatToDate.parse(orden.getCreatedAt());*/

        //item.setState(VerticalStepperItemView.STATE_NORMAL);

        if (orden != null && item.getState() != VerticalStepperItemView.STATE_SELECTED){
            item.setState(VerticalStepperItemView.STATE_DONE);
            item.setBackgroundColor(R.color.mdtp_accent_color);
        }

        SimpleDateFormat formatToText = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

        this.txtFechaIngreso.setText(formatToText.format(orden.getCreatedAt()));

        this.txtObservacion.setText(orden.getObservaciones());

        this.txtMecancio.setText(orden.getMecanico().getFullName());

        //onItemClickListener.OnCheckItemDone(index);
        /*itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.OnItemClick(0,v.getId());
            }
        });*/

        this.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.OnNextClick();
            }
        });

        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.OnGoToItem(index);
            }
        });
    }
}
