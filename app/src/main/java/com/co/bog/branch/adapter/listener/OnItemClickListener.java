package com.co.bog.branch.adapter.listener;

import com.co.bog.branch.ws.models.Cita;

public interface OnItemClickListener {
    void OnItemClick(Cita cita, int position, int layout);
}
