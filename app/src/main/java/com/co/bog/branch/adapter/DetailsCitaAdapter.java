package com.co.bog.branch.adapter;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.holder.CotizacionViewHolder;
import com.co.bog.branch.adapter.holder.GalleryViewHolder;
import com.co.bog.branch.adapter.holder.InfoCitaViewHolder;
import com.co.bog.branch.adapter.holder.IngresoViewHolder;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Orden;

import java.util.List;

import moe.feng.common.stepperview.IStepperAdapter;
import moe.feng.common.stepperview.VerticalStepperItemView;
import moe.feng.common.stepperview.VerticalStepperView;

public class DetailsCitaAdapter implements IStepperAdapter {

    private static final String LOG_TAG=DetailsCitaAdapter.class.getSimpleName();

    private OnItemCitaListener onItemCitaListener;

    private List<Orden> listOrdenes;

    private Cita cita;


    public DetailsCitaAdapter(Cita cita ,List<Orden> listOrdenes,OnItemCitaListener onItemCitaListener){
        this.cita = cita;
        this.listOrdenes = listOrdenes;
        this.onItemCitaListener = onItemCitaListener;
    }

    @NonNull
    @Override
    public CharSequence getTitle(int index) {
        switch (index){
            case 0:
                return "Ingreso";
            case 1:
                return "Diagnostico";
            case 2:
                return "Cotización";
            case 3:
                return "Aprobacion";
            case 4:
                return "Reparación";
            case 5:
                return "Entrega";
        }
        return "Step " + index;
    }

    @Nullable
    @Override
    public CharSequence getSummary(int index) {
        switch (index) {
            default:
                return null;
        }
    }

    @Override
    public int size() {
        return this.listOrdenes.size();
    }

    @Override
    public View onCreateCustomView(int index, Context context, VerticalStepperItemView parent) {
        //Log.d(LOG_TAG,"Show create Item ::>"+index);
        //Log.d(LOG_TAG,"Title create Item ::>"+parent.getTitle());
        View inflateView;
        switch (index){
            case 0:
                inflateView = LayoutInflater.from(context).inflate(R.layout.ingreso_item, parent, false);
                IngresoViewHolder ingresovh = new IngresoViewHolder(inflateView);
                ingresovh.bind(this.listOrdenes.get(index), index, parent, this.onItemCitaListener);
                break;
            case 1:
                inflateView = LayoutInflater.from(context).inflate(R.layout.fragment_gallery, parent, false);
                GalleryViewHolder diagnosticoViewHolder = new GalleryViewHolder(inflateView);
                diagnosticoViewHolder.bind(this.cita, index,parent, this.onItemCitaListener);
                break;
            case 2:
                inflateView = LayoutInflater.from(context).inflate(R.layout.cotizacion_item, parent, false);
                CotizacionViewHolder cotizacionViewHolder = new CotizacionViewHolder(inflateView);
                cotizacionViewHolder.bind(this.cita, index, parent, this.onItemCitaListener);
                break;
            case 3:
                inflateView = LayoutInflater.from(context).inflate(R.layout.cita_info_item, parent, false);
                InfoCitaViewHolder aprobacionvh = new InfoCitaViewHolder(inflateView);
                aprobacionvh.bind(this.listOrdenes.get(index), index,parent, this.onItemCitaListener);
                break;
            case 4:
                inflateView = LayoutInflater.from(context).inflate(R.layout.fragment_gallery, parent, false);
                GalleryViewHolder reparacionViewHolder = new GalleryViewHolder(inflateView);
                reparacionViewHolder.bind(this.cita, index,parent, this.onItemCitaListener);
                break;
            case 5:
                inflateView = LayoutInflater.from(context).inflate(R.layout.cotizacion_item, parent, false);
                CotizacionViewHolder entregaViewHolder = new CotizacionViewHolder(inflateView);
                entregaViewHolder.bind(this.cita, index, parent, this.onItemCitaListener);
                break;
            default:
                inflateView = null;
                break;
        }
        return inflateView;
    }

    @Override
    public void onShow(int index) {
        Log.d(LOG_TAG, "Show Item ::>"+index);
    }

    @Override
    public void onHide(int index) {
        Log.d(LOG_TAG, "Hide Item ::>"+index);
    }

    public interface OnItemCitaListener {

        void OnNextClick();

        void OnPrevClick();

        void OnGoToItem(int index);

        void viewDetailCotizacion(Cita cita, int indexEtapaPdf);

        void OnGoToDetailPhoto(Cita cita, int indexEtapaPdf , int position);
    }



}
