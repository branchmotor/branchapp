package com.co.bog.branch.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.co.bog.branch.R;
import com.co.bog.branch.ws.models.Marca;

import java.util.List;

public class MarcasReferenciaAdapter extends ArrayAdapter<Marca> {

    public MarcasReferenciaAdapter(@NonNull Context context,  @NonNull List<Marca> objects) {
        super(context, 0 , objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Marca marca = getItem(position);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.dropdown_referencias,parent,false);
        }

        TextView textReferencia = (TextView) convertView.findViewById(R.id.textReferenciaName);

        textReferencia.setText(marca.getReferencia());


        return convertView;
    }

    @Nullable
    @Override
    public Marca getItem(int position) {
        return super.getItem(position);
    }
}
