package com.co.bog.branch.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.co.bog.branch.R;
import com.co.bog.branch.ws.models.Documento;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class recyclerViewPagerImageAdapter extends RecyclerView.Adapter<recyclerViewPagerImageAdapter.ViewHolder> {

    private List<Documento> pictureList;

    private final OnItemClickListener itemClickListener;

    /**
     *
     * @param pictureList ArrayList of pictureFacer objects
     * @param itemClickListener Interface for communication between adapter and fragment
     */
    public recyclerViewPagerImageAdapter(List<Documento> pictureList,  OnItemClickListener itemClickListener) {
        this.pictureList = pictureList;
        this.itemClickListener = itemClickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.indicator_holder, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(pictureList.get(position), position,itemClickListener);
    }


    @Override
    public int getItemCount() {
        return this.pictureList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView image;

        private View positionController;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.image = (ImageView) itemView.findViewById(R.id.imageIndicator);
            this.positionController = (View) itemView.findViewById(R.id.activeImage);
        }

        public void bind(final Documento documento,int position,final OnItemClickListener onItemClickListener){

            //Ajusta el background de las tarjetas inferirores en la galeria
            this.positionController.setBackgroundColor(documento.getSelected() ? Color.parseColor("#00000000") : Color.parseColor("#8c000000"));

            Picasso.get().load(documento.getUrlThumbnail())
                    .into(this.image);

            this.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //holder.card.setCardElevation(5);
                    documento.setSelected(true);
                    notifyDataSetChanged();
                    onItemClickListener.OnItemClick(position);
                }
            });
        }


    }

    public interface OnItemClickListener {
        void OnItemClick(int ImagePosition);
    }
}
