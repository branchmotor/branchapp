package com.co.bog.branch.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.co.bog.branch.R;

import com.co.bog.branch.util.Util;
import com.co.bog.branch.ws.models.Usuario;
import com.co.bog.branch.ws.service.BranchService;
import com.co.bog.branch.ws.ws;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity{

    private static final int RC_SIGN_IN = 123;
    private static final String LOG_TAG = LoginActivity.class.getSimpleName();

    private SharedPreferences prefs;

    private SharedPreferences.Editor mEditor;

    private EditText editTextEmail;
    private EditText editTextPassword;
    private Switch switchRemember;
    private Button buttonLogin;
    private Button buttonCreateAccount;

    private FirebaseAuth mAuth;

    private static final String NAME_SHARED_PREFERENCES="prefsBranchs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        prefs =  getBaseContext().getSharedPreferences(NAME_SHARED_PREFERENCES,MODE_PRIVATE);
        mEditor = prefs.edit();

        createSignInIntent();

    }

    public void createSignInIntent() {
        // [START auth_fui_create_intent]
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.FacebookBuilder().build());

        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setIsSmartLockEnabled(false)
                        .setTheme(R.style.LoginTheme)
                        .setLogo(R.drawable.ic_logo_neg_con)
                        .build(),
                RC_SIGN_IN);
        // [END auth_fui_create_intent]
    }


    // [START auth_fui_result]
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                BranchService service = ws.getService(this).create(BranchService.class);

                Call<Usuario> usuarioCall = service.findUsuarioByEmail(user.getEmail());

                usuarioCall.enqueue(new Callback<Usuario>() {
                    @Override
                    public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                        if (response.isSuccessful()) {
                            Usuario usuario = response.body();
                            Log.d(LOG_TAG,"Valor del usuario respuesta WS ::::>" + usuario.toString());
                            if(usuario.getIdUsuario() == null){


                                Usuario usuarioNuevo = new Usuario();

                                usuarioNuevo.setEmail(user.getEmail());
                                usuarioNuevo.setFirstName(user.getDisplayName());
                                usuarioNuevo.setUid(user.getUid());
                                usuarioNuevo.setCelular(user.getPhoneNumber());
                                usuarioNuevo.setTipoUsuario("Cliente");

                                mEditor.putString(getString(R.string.prefTipoUsuario), usuario.getTipoUsuario());
                                mEditor.putString(getString(R.string.prefIdentificacion), usuarioNuevo.getIdentificacion());
                                mEditor.commit();


                                Log.d(LOG_TAG,"Usuario to send WS ::::>" + usuarioNuevo.toString());


                                Call<Usuario> usuarioCall = service.crearUsuario(usuarioNuevo);

                                usuarioCall.enqueue(new Callback<Usuario>() {
                                    @Override
                                    public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                                        if (response.isSuccessful()) {
                                            Log.d(LOG_TAG,"Se creo correctamente el usuario el la BD de usuarios ");
                                            goToMain();
                                        }else {
                                            int statusCode = response.code();
                                            Log.e(LOG_TAG, "onResponse(): Error al crear usuario el la BD de Usuarios = " + statusCode);
                                            Intent intentLogin = new Intent(getApplicationContext(), LoginActivity.class);
                                            startActivity(intentLogin);
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Usuario> call, Throwable t) {
                                        Log.e(LOG_TAG, "Error al crear usuario en la BD = " + t.getMessage());
                                        Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }else{

                                Log.d(LOG_TAG,"Identificacion login ::>"+usuario.getIdentificacion());
                                mEditor.putString(getString(R.string.prefTipoUsuario), usuario.getTipoUsuario());
                                mEditor.putString(getString(R.string.prefIdentificacion), usuario.getIdentificacion());
                                mEditor.commit();
                                goToMain();
                            }
                    } else {
                            int statusCode = response.code();
                            Log.e(LOG_TAG, "onResponse(): Error al obtener usuario por Email = " + statusCode);
                        }
                    }

                    @Override
                    public void onFailure(Call<Usuario> call, Throwable t) {
                        //Toast.makeText(MotosFragment.this,"Error", Toast.LENGTH_SHORT).show();
                        Log.e(LOG_TAG, "Error al obtener usuario By Email = " + t.getMessage());
                        Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });


                // ...
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }


    private void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }




}
