package com.co.bog.branch.fragments;


import android.Manifest;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.co.bog.branch.R;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.co.bog.branch.util.Validacion;
import com.co.bog.branch.ws.models.Documento;
import com.co.bog.branch.ws.models.Marca;
import com.co.bog.branch.ws.models.Usuario;
import com.co.bog.branch.ws.models.Vehiculo;
import com.co.bog.branch.ws.service.BranchService;
import com.co.bog.branch.ws.ws;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateVehiclesFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener, AdapterView.OnItemClickListener {

    private static final String LOG_TAG = CreateVehiclesFragment.class.getSimpleName();

    private SwitchFragmentListener firstPageListener;

    private TextInputEditText textInputPlaca;

    private AutoCompleteTextView autoCompleteMarca;

    private AutoCompleteTextView autoCompleteReferencia;

    private TextInputEditText textInputKilometraje;

    private TextInputEditText textInputColor;

    private TextInputEditText textInputFechaCompra;

    private TextInputLayout layoutFechaCompra;

    private TextInputEditText textInputApodo;

    private AutoCompleteTextView autoCompleteYearsModel;

    private MaterialButton btnCreateVehicle;

    private DatePickerDialog dpd;

    private MaterialCardView layoutPhotVehiculo;

    private ImageView photoVehiculo;

    private ProgressBar progressBarPhoto;

    private FirebaseAuth mAuth;

    private Vehiculo vehiculo;

    private View view;

    private String currentPhotoPath;

    private static final int REQUEST_IMAGE_CAPTURE = 100;

    private static final int REQUEST_IMAGE_GALERY = 200;

    private static final int STORAGE_PERMISSION_CODE = 300;

    private static final int CAMERA_PERMISSION_CODE = 400;


    public CreateVehiclesFragment(SwitchFragmentListener listener, Vehiculo vehiculo) {
        // Required empty public constructor
        firstPageListener = listener;
        this.vehiculo = vehiculo;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_create_vehicles, container, false);
        bindUI(view);
        return view;
    }

    public void bindUI(View view) {

        mAuth = FirebaseAuth.getInstance();

        this.layoutPhotVehiculo = (MaterialCardView) view.findViewById(R.id.layoutImageVehiculo);
        this.photoVehiculo = (ImageView) view.findViewById(R.id.photoVehiculo);



        this.textInputPlaca = (TextInputEditText) view.findViewById(R.id.txtCreatePlaca);
        this.textInputKilometraje = (TextInputEditText) view.findViewById(R.id.txtCreateKilometraje);
        this.autoCompleteMarca = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteMarca);
        this.autoCompleteReferencia = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteReferencia);
        this.textInputColor = (TextInputEditText) view.findViewById(R.id.txtCreateColor);
        this.textInputApodo = (TextInputEditText) view.findViewById(R.id.txtCreateApodo);
        this.textInputFechaCompra = (TextInputEditText) view.findViewById(R.id.txtCreateFechaCompra);
        this.autoCompleteYearsModel = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(), R.array.years_array, R.layout.dropdown_menu_popup_item);
        // Apply the adapter to the spinner
        autoCompleteYearsModel.setAdapter(adapter);



        if (vehiculo != null) {
            this.textInputPlaca.setText(vehiculo.getPlaca());
            this.textInputPlaca.setEnabled(false);
            this.textInputKilometraje.setText(vehiculo.getKilometraje().toString());
            this.textInputColor.setText(vehiculo.getColor());
            if(vehiculo.getColor()!= null && !vehiculo.getColor().isEmpty()){
                this.textInputColor.setEnabled(false);
            }
            this.textInputApodo.setText(vehiculo.getAlias());

            if(vehiculo.getFechaCompra() != null){
                SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyy");
                this.textInputFechaCompra.setText(simpledateformat.format(vehiculo.getFechaCompra()));
                this.textInputFechaCompra.setEnabled(false);
            }

            if(vehiculo.getModelo() != null){
                this.autoCompleteYearsModel.setText(Integer.toString(vehiculo.getModelo()) ,false);
            }


            if(vehiculo.getModelo() != null && vehiculo.getModelo()!=0){
                this.autoCompleteYearsModel.setEnabled(false);
            }
        }

        loadPhotoField();

        BranchService service = ws.getService(getContext()).create(BranchService.class);

        Call<List<Marca>> marcaCall = service.listUniqueMarcas();

        marcaCall.enqueue(new Callback<List<Marca>>() {
            @Override
            public void onResponse(Call<List<Marca>> call, Response<List<Marca>> response) {
                if (response.isSuccessful()) {
                    //Toast.makeText(getActivity(), "Se creo el vehiculo correctamente", Toast.LENGTH_LONG).show();

                    List<Marca> marcas = response.body();
                    List<String> textMarcas = new ArrayList<>();
                    for (Marca marca : marcas) {
                        textMarcas.add(marca.getMarca());
                    }

                    Log.d(LOG_TAG, " Listado de marcada " + marcas);

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(), R.layout.dropdown_menu_popup_item, textMarcas);

                    autoCompleteMarca.setAdapter(adapter);

                    if(vehiculo != null){
                        //int position = adapter.getPosition(vehiculo.getMarca().getMarca());
                        //Log.d(LOG_TAG," Psocion de la marca "+vehiculo.getMarca().getMarca()+" es :::>"+position);
                        autoCompleteMarca.setText(vehiculo.getMarca().getMarca(),false);
                        if(vehiculo.getMarca().getIdMarca() != 1){
                            autoCompleteMarca.setEnabled(false);
                        }
                        loadReferencias(view,vehiculo.getMarca().getMarca());
                    }

                } else {
                    int statusCode = response.code();
                    Log.e(LOG_TAG, "onResponse(): Error code = " + statusCode + " Mensaje = " + response.message());
                }

            }

            @Override
            public void onFailure(Call<List<Marca>> call, Throwable t) {
                Log.e(LOG_TAG, " Error al listar marcas " + t.getMessage());
                Toast.makeText(getActivity(), "Fallo al list marcas", Toast.LENGTH_LONG).show();
            }
        });

        this.autoCompleteMarca.setOnItemClickListener(this);

        this.layoutFechaCompra = (TextInputLayout) view.findViewById(R.id.layoutTextFechaCompra);
        this.btnCreateVehicle = (MaterialButton) view.findViewById(R.id.btnCreateVehicle);


        this.textInputFechaCompra.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showCalendarPicker();
                } else {
                    Log.d("DatePickerDialog", "Dialog was cancelled");
                    dpd = null;
                }
            }
        });

        this.layoutFechaCompra.setOnClickListener(this);
        this.textInputFechaCompra.setOnClickListener(this);
        this.btnCreateVehicle.setOnClickListener(this);
        this.layoutPhotVehiculo.setOnClickListener(this);

        if(vehiculo != null){
            this.btnCreateVehicle.setText(R.string.lblActualizar);
        }
    }

    private void loadPhotoField() {

        if(this.progressBarPhoto == null){
            this.progressBarPhoto = (ProgressBar) view.findViewById(R.id.progressBarPhoto);
        }

        this.progressBarPhoto.setVisibility(View.VISIBLE);

        if(vehiculo != null && vehiculo.getFotos().size() > 0){
            Picasso.get()
                    .load(vehiculo.getFotos().get(0).getUrl())
                    .fit()
                    .centerCrop()
                    .into(this.photoVehiculo, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    Log.d(LOG_TAG,"Cargo la Foto correctamente");
                    if (progressBarPhoto != null) {
                        progressBarPhoto.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onError(Exception e) {

                }
            });
        }else{
            Picasso.get().load(R.drawable.bg_load_photo)
                    .fit()
                    .centerCrop()
                    .into(this.photoVehiculo);
            if (progressBarPhoto != null) {
                progressBarPhoto.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public void onClick(View v) {
        Log.d(LOG_TAG, "Layout click test >>>>" + v.getId());
        switch (v.getId()) {
            case R.id.btnCreateVehicle:
                createVehicle();
                break;
            case R.id.layoutImageVehiculo:
                selectedImage();
                break;
        }
    }

    /**
     *
     */
    private void createVehicle() {
        if (checkValidation()) {

            if(this.vehiculo == null){
                this.vehiculo = new Vehiculo();
            }

            this.vehiculo.setPlaca(this.textInputPlaca.getText().toString());
            this.vehiculo.setKilometraje(Integer.valueOf(this.textInputKilometraje.getText().toString()));
            this.vehiculo.setModelo(Integer.parseInt(this.autoCompleteYearsModel.getText().toString()));
            this.vehiculo.setColor(this.textInputColor.getText().toString());
            this.vehiculo.setAlias(this.textInputApodo.getText().toString());

            Marca marca = new Marca();
            marca.setMarca(this.autoCompleteMarca.getText().toString());
            marca.setReferencia(this.autoCompleteReferencia.getText().toString());

            this.vehiculo.setMarca(marca);

            FirebaseUser user = mAuth.getCurrentUser();

            this.vehiculo.setUsuario(new Usuario(user.getEmail(), user.getUid(), user.getPhoneNumber()));
            this.vehiculo.setTipoVehiculo("Moto");
            this.vehiculo.setEstado("Pendiente");

            try {
                SimpleDateFormat simpledateformat = new SimpleDateFormat("d/M/yyyy");
                this.vehiculo.setFechaCompra(simpledateformat.parse(this.textInputFechaCompra.getText().toString()));
                this.vehiculo.setFechaCompraText(this.textInputFechaCompra.getText().toString());
            } catch (ParseException e) {
                Log.e(LOG_TAG, "Error al parsear fecha de compra vehiculo, la fecha a parsear es " + this.textInputFechaCompra.getText().toString());
                this.vehiculo.setFechaCompra(new Date());
            }

            BranchService service = ws.getService(getContext()).create(BranchService.class);

            //Se va a crear un nuevo vehiculo
            if(this.vehiculo.getIdVehiculo() == null){
                Call<Vehiculo> vehiculoCall = service.crearVehiculo(this.vehiculo);

                vehiculoCall.enqueue(new Callback<Vehiculo>() {
                    @Override
                    public void onResponse(Call<Vehiculo> call, Response<Vehiculo> response) {
                        if (response.isSuccessful()) {
                            Snackbar.make(view, R.string.msgSuccessCreateVehicle, Snackbar.LENGTH_LONG)
                                    .show();
                            //Toast.makeText(getActivity(), "Se creo el vehiculo correctamente", Toast.LENGTH_LONG).show();
                            firstPageListener.onSwitchVehicleFragment(null);
                        } else {
                            int statusCode = response.code();
                            Log.e(LOG_TAG, "onResponse(): Error code = " + statusCode + " Mensaje = " + response.message() + " Error ::>" + response.errorBody().toString());
                            Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Vehiculo> call, Throwable t) {
                        Log.e(LOG_TAG, " Error al crear vehiculo " + t.getMessage());
                        //Toast.makeText(getActivity(), "Fallo al crear vehiculo", Toast.LENGTH_LONG).show();
                        Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                                .show();
                    }
                });
            }else{
                //Se va a actualizar el vehiculo
                Call<Object> vehiculoCall = service.updateVehiculo(this.vehiculo.getIdVehiculo(),this.vehiculo);

                vehiculoCall.enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        if (response.isSuccessful()) {
                            Snackbar.make(view, R.string.msgSuccessUpdateVehicle, Snackbar.LENGTH_LONG)
                                    .show();
                            firstPageListener.onSwitchVehicleFragment(null);
                        } else {
                            int statusCode = response.code();
                            Log.e(LOG_TAG, "onResponse(): Error code = " + statusCode + " Mensaje = " + response.message() + " Error ::>" + response.errorBody().toString());
                            Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        Log.e(LOG_TAG, " Error al actualizar vehiculo " + t.getMessage());
                        Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                                .show();
                    }
                });

            }



        } else {
            Snackbar.make(view, R.string.msgFormFailed, Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    private void showCalendarPicker() {
        Calendar now = Calendar.getInstance();

            /*if (defaultSelection.isChecked()) {
                now.add(Calendar.DATE, 7);
            }*/

            /*
            It is recommended to always create a new instance whenever you need to show a Dialog.
            The sample app is reusing them because it is useful when looking for regressions
            during testing
             */
        if (dpd == null) {
            dpd = DatePickerDialog.newInstance(
                    CreateVehiclesFragment.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
        } else {
            dpd.initialize(
                    CreateVehiclesFragment.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
        }
        //dpd.setThemeDark(modeDarkDate.isChecked());
        //dpd.vibrate(vibrateDate.isChecked());
        //dpd.dismissOnPause(dismissDate.isChecked());
        dpd.showYearPickerFirst(true);
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        //dpd.setVersion(showVersion2.isChecked() ? DatePickerDialog.Version.VERSION_2 : DatePickerDialog.Version.VERSION_1);
        //if (modeCustomAccentDate.isChecked()) {
        //dpd.setAccentColor(Color.parseColor("#9C27B0"));
        //}
        //if (titleDate.isChecked()) {
        dpd.setTitle("Fecha de compra");
        //}
            /*if (highlightDays.isChecked()) {
                Calendar date1 = Calendar.getInstance();
                Calendar date2 = Calendar.getInstance();
                date2.add(Calendar.WEEK_OF_MONTH, -1);
                Calendar date3 = Calendar.getInstance();
                date3.add(Calendar.WEEK_OF_MONTH, 1);
                Calendar[] days = {date1, date2, date3};
                dpd.setHighlightedDays(days);
            }*/
            /*if (limitSelectableDays.isChecked()) {
                Calendar[] days = new Calendar[13];
                for (int i = -6; i < 7; i++) {
                    Calendar day = Calendar.getInstance();
                    day.add(Calendar.DAY_OF_MONTH, i * 2);
                    days[i + 6] = day;
                }
                dpd.setSelectableDays(days);
            }*/
            /*if (switchOrientation.isChecked()) {
                if (dpd.getVersion() == DatePickerDialog.Version.VERSION_1) {
                    dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.HORIZONTAL);
                } else {
                    dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                }
            }*/
        dpd.setOnCancelListener(dialog -> {
            Log.d("DatePickerDialog", "Dialog was cancelled");
            dpd = null;
        });
        dpd.show(requireFragmentManager(), "Datepickerdialog");
    }

    /**
     * @return
     */
    private boolean checkValidation() {
        boolean ret = true;

        if (!Validacion.hasText(this.textInputPlaca)) ret = false;
        if (!Validacion.hasText(this.textInputKilometraje)) ret = false;
        if (!Validacion.hasText(this.autoCompleteYearsModel)) ret = false;
        if (!Validacion.hasText(this.textInputColor)) ret = false;
        if (!Validacion.hasText(this.textInputFechaCompra)) ret = false;
        if (!Validacion.hasText(this.textInputApodo)) ret = false;

        return ret;
    }

    public void backPressed() {
        firstPageListener.onSwitchVehicleFragment(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(LOG_TAG, "Activity Result  RequestCode ::> " + requestCode + " ResultCode ::> " + resultCode + " Data ::>" + data);
        if(this.progressBarPhoto == null){
            this.progressBarPhoto = (ProgressBar) view.findViewById(R.id.progressBarPhoto);
        }

        this.progressBarPhoto.setVisibility(View.VISIBLE);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            Log.d(LOG_TAG, "Se va a cargar de la URL " + currentPhotoPath);

            //Uri selectedImageURI = Uri.parse(currentPhotoPath);
            File file = new File(currentPhotoPath);

            // Inicializar el proveedor de credenciales de Amazon Cognito
            uploadToS3(file);



        } else {
            if (requestCode == REQUEST_IMAGE_GALERY && resultCode == RESULT_OK) {

                Uri selectedImage = data.getData();

                String[] filePath = { MediaStore.Images.Media.DATA };

                Log.d(LOG_TAG,"Path image load first File path "+filePath[0]);

                Cursor c = getActivity().getContentResolver().query(selectedImage,filePath, null, null, null);

                c.moveToFirst();

                int columnIndex = c.getColumnIndex(filePath[0]);

                String picturePath = c.getString(columnIndex);

                Log.d(LOG_TAG,"Path image load form Gallery "+picturePath);

                c.close();

                uploadToS3(new File(picturePath));

                /*
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                thumbnail=getResizedBitmap(thumbnail, 400);
                Log.w(LOG_TAG, picturePath+"");
                //IDProf.setImageBitmap(thumbnail);
                BitMapToString(thumbnail);*/
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void selectedImage() {

        final CharSequence[] options = {getString(R.string.lblTomarPhoto), getString(R.string.lblBuscarPhoto), getString(R.string.lblCancelar)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.lblMuestraPhoto)
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        if (options[which].equals(getString(R.string.lblTomarPhoto))) {

                            dispatchTakePictureIntent();

                        } else if (options[which].equals(getString(R.string.lblBuscarPhoto))) {
                            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, REQUEST_IMAGE_GALERY);
                        } else if (options[which].equals(getString(R.string.lblCancelar))) {
                            dialog.dismiss();

                        }
                    }
                }).show();
    }

    private void dispatchTakePictureIntent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        // Error occurred while creating the File

                    }
                    Log.d(LOG_TAG, "File result :::>" + photoFile.getAbsolutePath());


                    // Continue only if the File was successfully created
                    if (photoFile != null) {

                        Uri photoURI = FileProvider.getUriForFile(getActivity(),
                                "com.mybranch.fileprovider",
                                photoFile);
                        Log.d(LOG_TAG, "Photo URI :::>" + photoURI);

                        //this.vehiculo = vehiculo;
                        //takePictureIntent.putExtra("IdVehiculo", idvehiculo);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            } else {
                Log.d(LOG_TAG, " No tiene permisos los va a solicitar");

                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Log.d(LOG_TAG, " Access fine location request permission before");
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                } else {
                    Log.d(LOG_TAG, " Ya denego los permisos los va a solicitar");
                    // Ha denegado
                    Toast.makeText(getContext(), "Por favor configure los permisos de ubicacion para poder usar este modulo :::> ", Toast.LENGTH_LONG).show();
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                }
            }
        }

    }

    private boolean checkPermission(String permission) {
        int result = getActivity().checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        String permission = permissions[0];
        int result = grantResults[0];
        switch (requestCode) {
            case CAMERA_PERMISSION_CODE:

                if (permission.equals(Manifest.permission.CAMERA)) {
                    if (result == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getActivity(), "Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();
                }
                break;
            case STORAGE_PERMISSION_CODE:
                if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Comprobar si ha sido aceptado el permiso
                    if (result == PackageManager.PERMISSION_GRANTED) {
                        // Concedio el permiso
                        Log.d(LOG_TAG, " Se dio el permiso a storage ");

                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

                            // Create the File where the photo should go
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch (IOException ex) {
                                // Error occurred while creating the File

                            }
                            Log.d(LOG_TAG, "File result :::>" + photoFile.getAbsolutePath());


                            // Continue only if the File was successfully created
                            if (photoFile != null) {
                                Uri mPhotoUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
                        /*Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.mybranch.fileprovider",
                        photoFile);*/
                                Log.d(LOG_TAG, "Photo URI :::>" + mPhotoUri);

                                //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                            }
                        }

                    } else {
                        // No concedio el permiso
                        Toast.makeText(getContext(), "No doy permiso", Toast.LENGTH_LONG).show();
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void uploadToS3(File file) {

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getActivity().getApplicationContext(),
                "us-east-2:2b36dfa2-1fe0-400a-82cb-cd167a14cc3a", // ID del grupo de identidades
                Regions.US_EAST_2 // Región
        );

        Log.d(LOG_TAG," File get name key to s3 :::>"+file.getName());

        AmazonS3 s3 = new AmazonS3Client(credentialsProvider);

        String extension = file.getName().substring(file.getName().lastIndexOf("."));

        String key = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())+extension;

        TransferUtility transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());
        //transferUtility.upload()
        final TransferObserver observer = transferUtility.upload(
                "branchmedia",  //this is the bucket name on S3
                key, //this is the path and name
                //file.getName(),
                file, //path to the file locally
                CannedAccessControlList.PublicRead //to make the file public
        );

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.equals(TransferState.COMPLETED)) {
                    //Success
                    try {

                        if(vehiculo == null){
                            vehiculo = new Vehiculo();
                        }

                        Log.d(LOG_TAG,"Se subio el archivo exitosamente to S3 ::>"+state.name());

                        String url = "https://branchmedia.s3.us-east-2.amazonaws.com/"+key;

                        List<Documento> arrayFotos = new ArrayList<Documento>();

                        Documento foto = new Documento();

                        foto.setUrl(url);

                        Calendar now = Calendar.getInstance();

                        foto.setDate(now.getTime().toString());

                        foto.setSize((int) file.length());

                        URLConnection connection = file.toURL().openConnection();

                        String mimeType = connection.getContentType();

                        foto.setType(mimeType);

                        foto.setNombreArchivo(file.getName());

                        foto.setKeynameFile(key);

                        arrayFotos.add(foto);

                        Log.d(LOG_TAG," ArrayFotos Result ::::>"+arrayFotos);

                        vehiculo.setFotos(arrayFotos);

                        loadPhotoField();

                    }catch (MalformedURLException e){
                        Log.e(LOG_TAG,"Error MalFormed URL "+e.getMessage());
                    }catch (IOException e){
                        Log.e(LOG_TAG,"Error IO File "+e.getMessage());
                    }


                } else if (state.equals(TransferState.FAILED)) {
                    //Failed
                    Log.d(LOG_TAG,"Error al subir archivo to S3");
                }

            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                Log.d(LOG_TAG,"Progress Changed, bytesCurrent ::> "+bytesCurrent+ " Total ::>"+bytesTotal);
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.d(LOG_TAG,"Error en S3 "+ex.getMessage());
            }
        });
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_VEHICULO_PHOTO_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        Log.d(LOG_TAG, " Storage dir ::::>" + storageDir);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        Log.d(LOG_TAG, " Current photo ::> " + currentPhotoPath);
        return image;
    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) requireFragmentManager().findFragmentByTag("Datepickerdialog");
        if (dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dpd = null;
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        Log.d(LOG_TAG,"Dia seleccinado :::> " + date);
        this.textInputFechaCompra.setText(date);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Object item = parent.getItemAtPosition(position);
        if (item instanceof String) {
            String marca = (String) item;
            Log.i(LOG_TAG, "Item selected :::> " + marca);
            loadReferencias(view, marca);
        }
    }

    private void loadReferencias(View view, String marca) {

        autoCompleteReferencia.setText("",false);

        BranchService service = ws.getService(getContext()).create(BranchService.class);

        Call<List<Marca>> marcaCall = service.listMarcas(marca);

        marcaCall.enqueue(new Callback<List<Marca>>() {
            @Override
            public void onResponse(Call<List<Marca>> call, Response<List<Marca>> response) {
                if (response.isSuccessful()) {
                    //Toast.makeText(getActivity(), "Se creo el vehiculo correctamente", Toast.LENGTH_LONG).show();
                    //firstPageListener.onSwitchVehicleFragment();
                    List<Marca> marcas = response.body();

                    List<String> textReferencias = new ArrayList<>();
                    for (Marca marca : marcas) {
                        textReferencias.add(marca.getReferencia());
                    }
                    //Log.d(LOG_TAG," Listado de marcada "+ marcas);

                    //MarcasReferenciaAdapter adapter = new MarcasReferenciaAdapter(view.getContext(),marcas);

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(), R.layout.dropdown_menu_popup_item, textReferencias);

                    autoCompleteReferencia.setAdapter(adapter);

                    if(vehiculo != null && vehiculo.getIdVehiculo() != null && vehiculo.getMarca().getMarca().equals(marca)){
                        //int position = adapter.getPosition(vehiculo.getMarca().getMarca());
                        //Log.d(LOG_TAG," Psocion de la marca "+vehiculo.getMarca().getMarca()+" es :::>"+position);
                        autoCompleteReferencia.setText(vehiculo.getMarca().getReferencia(),false);
                        if(vehiculo.getMarca().getIdMarca() != 1){
                            autoCompleteReferencia.setEnabled(false);
                        }

                    }

                } else {
                    int statusCode = response.code();
                    Log.e(LOG_TAG, "onResponse(): Error code = " + statusCode + " Mensaje = " + response.message());
                    Toast.makeText(getActivity(), "Fallo al list referencias", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<List<Marca>> call, Throwable t) {
                Log.e(LOG_TAG, " Error al listar referencias " + t.getMessage());
                Toast.makeText(getActivity(), "Fallo al list referencias", Toast.LENGTH_LONG).show();
            }
        });
        // do something with the studentInfo object
    }
}
