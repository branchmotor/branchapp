package com.co.bog.branch.adapter.holder;

import android.view.View;
import android.widget.TextView;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.DetailsCitaAdapter;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Orden;
import com.google.android.material.button.MaterialButton;

import java.text.SimpleDateFormat;

import moe.feng.common.stepperview.VerticalStepperItemView;

public class CotizacionViewHolder {

    private static final String LOG_TAG = CotizacionViewHolder.class.getSimpleName();

    private MaterialButton buttonViewCotizacion;

    private static final int STORAGE_PERMISSION_CODE = 300;

    private View view;

    private TextView textFecha;

    private TextView textMecanico;

    private Orden orden;


    public CotizacionViewHolder(View view) {

        this.buttonViewCotizacion = (MaterialButton) view.findViewById(R.id.buttonViewCotizacion);

        this.textFecha = (TextView) view.findViewById(R.id.textFecha);

        this.textMecanico = (TextView) view.findViewById(R.id.textMecancio);

        this.view = view;
    }

    public void bind(Cita cita, int index, VerticalStepperItemView item, final DetailsCitaAdapter.OnItemCitaListener onItemClickListener) {

        if (cita != null && item.getState() != VerticalStepperItemView.STATE_SELECTED){
            item.setState(VerticalStepperItemView.STATE_DONE);
        }

        this.orden = cita.getOrdenes().get(index);

        SimpleDateFormat formatToText = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

        this.textFecha.setText(formatToText.format(this.orden.getCreatedAt()));

        this.textMecanico.setText(orden.getMecanico().getFullName());

        this.buttonViewCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.viewDetailCotizacion(cita, 2);
            }
        });

        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.OnGoToItem(index);
            }
        });


    }

}
