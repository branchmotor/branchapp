package com.co.bog.branch.adapter;

import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.co.bog.branch.R;
import com.co.bog.branch.activities.MainActivity;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.co.bog.branch.fragments.CitasFragment;
import com.co.bog.branch.fragments.CotizacionFragment;
import com.co.bog.branch.fragments.CreateVehiclesFragment;
import com.co.bog.branch.fragments.DetailCitaFragment;
import com.co.bog.branch.fragments.DocumentoVehiculoFragment;
import com.co.bog.branch.fragments.GalleryCitasFragment;
import com.co.bog.branch.fragments.MotosFragment;
import com.co.bog.branch.fragments.NewCitaFragment;
import com.co.bog.branch.fragments.TalleresFragment;
import com.co.bog.branch.fragments.UpdateUserFragment;
import com.co.bog.branch.fragments.UserFragment;
import com.co.bog.branch.fragments.pictureBrowserFragment;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Vehiculo;

public class PageAdapter extends FragmentStatePagerAdapter  {

    //private final List<Fragment> mFragmentList = new ArrayList<>();

    private static final String LOG_TAG = PageAdapter.class.getSimpleName();

    private FragmentManager mFragmentManager;

    public Fragment mFragmentAtPos0;

    public Fragment mFragmentAtPos4;

    public Fragment mFragmentAtPos3;

    private SwitchPageListener listener = new SwitchPageListener();

    private MainActivity.SwitchToolbarTitleListener switchToolbartitle;

    public PageAdapter(FragmentManager fm, MainActivity.SwitchToolbarTitleListener switchToolbartitle) {
        super(fm);
        this.mFragmentManager = fm;
        this.switchToolbartitle = switchToolbartitle;
    }

    private final class SwitchPageListener implements  SwitchFragmentListener {

        ///////////////////////////////////////////////////////////////////////////////////
        // Fragments de vehiculos
        ///////////////////////////////////////////////////////////////////////////////////////
        @Override
        public void onSwitchVehicleFragment(Vehiculo vehiculo) {
            Log.d(LOG_TAG,"Haciendo el switch de fragments paso 1");
            mFragmentManager.beginTransaction().detach(mFragmentAtPos0)
                    .commit();


            if (mFragmentAtPos0 instanceof MotosFragment) {
                Log.d(LOG_TAG,"Haciendo el switch de fragments paso 2");

                //Set the title
                if(vehiculo == null){
                    switchToolbartitle.onSwitchFragment(R.string.nuevamoto);
                }else{
                    switchToolbartitle.onSwitchFragment(R.string.editarmoto);
                }

                mFragmentAtPos0 = new CreateVehiclesFragment(listener, vehiculo);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos0).commit();

            } else { // Instance of NextFragment
                switchToolbartitle.onSwitchFragment(R.string.motos);
                mFragmentAtPos0 = new MotosFragment(listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos0).commit();
            }
            notifyDataSetChanged();
        }

        @Override
        public void onSwitchVehicleDocumentosFragment(Vehiculo vehiculo) {

            mFragmentManager.beginTransaction().detach(mFragmentAtPos0)
                    .commit();

            if (mFragmentAtPos0 instanceof MotosFragment) {
                Log.d(LOG_TAG,"Haciendo el switch de fragments paso 2");
                mFragmentAtPos0 = new DocumentoVehiculoFragment(listener, vehiculo);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos0).commit();

            } else { // Instance of NextFragment

                mFragmentAtPos0 = new MotosFragment(listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos0).commit();
            }
            notifyDataSetChanged();
        }

        ///////////////////////////////////////////////////////////////////////////////////
        // Fragments de usuarios
        ////////////////////////////////////////////////////////////////////////////////////
        @Override
        public void onSwitchUserFragment() {
            Log.d(LOG_TAG,"Haciendo el switch de fragments paso 1");
            mFragmentManager.beginTransaction().detach(mFragmentAtPos3)
                    .commit();

            if (mFragmentAtPos3 instanceof UserFragment) {
                Log.d(LOG_TAG,"Haciendo el switch de fragments paso 2");
                mFragmentAtPos3 = new UpdateUserFragment(listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos3).commit();

            } else { // Instance of NextFragment
                mFragmentAtPos3 = new UserFragment(listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos3).commit();
            }
            notifyDataSetChanged();
        }


        ///////////////////////////////////////////////////////////////////////////////////
        // Fragments de citas
        ///////////////////////////////////////////////////////////////////////////////////////
        @Override
        public void onSwitchCitaFragment() {
            Log.d(LOG_TAG,"Haciendo el switch de fragments paso 1");
            mFragmentManager.beginTransaction().detach(mFragmentAtPos4)
                    .commit();

            if (mFragmentAtPos4 instanceof CitasFragment) {
                Log.d(LOG_TAG,"Haciendo el switch de fragments paso 2");

                switchToolbartitle.onSwitchFragment(R.string.nuevacita);

                mFragmentAtPos4 = new NewCitaFragment(listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos4).commit();

            } else { // Instance of NextFragment

                switchToolbartitle.onSwitchFragment(R.string.citas);

                mFragmentAtPos4 = new CitasFragment(listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos4).commit();
            }
            notifyDataSetChanged();
        }

        @Override
        public void onSwitchGalleryFragment(Cita cita) {
            mFragmentManager.beginTransaction().detach(mFragmentAtPos4)
                    .commit();

            if (mFragmentAtPos4 instanceof CitasFragment) {
                Log.d(LOG_TAG,"Haciendo el switch de fragments paso 2 de la galeria");
                mFragmentAtPos4 = new GalleryCitasFragment(cita, listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos4).commit();

            } else { // Instance of NextFragment
                mFragmentAtPos4 = new CitasFragment(listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos4).commit();
            }
            notifyDataSetChanged();
        }

        @Override
        public void onSwitchDetailCita(Cita cita) {
            mFragmentManager.beginTransaction().detach(mFragmentAtPos4)
                    .commit();

            if (mFragmentAtPos4 instanceof CitasFragment) {

                switchToolbartitle.onSwitchFragment(R.string.detallecita);

                mFragmentAtPos4 = new DetailCitaFragment(cita, listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos4).commit();

            } else { // Instance of NextFragment

                switchToolbartitle.onSwitchFragment(R.string.citas);

                mFragmentAtPos4 = new CitasFragment(listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos4).commit();
            }
            notifyDataSetChanged();
        }

        @Override
        public void onSwitchVisualizadorPDF(Cita cita) {
            mFragmentManager.beginTransaction().detach(mFragmentAtPos4)
                    .commit();

            if (mFragmentAtPos4 instanceof DetailCitaFragment) {
                //Log.d(LOG_TAG,"Haciendo el switch de fragments a la cotizacion ::::>"+orden.getDocumentos().get(0).getKeynameFile());
                mFragmentAtPos4 = new CotizacionFragment(cita, listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos4).commit();

            } else { // Instance of NextFragment
                mFragmentAtPos4 = new DetailCitaFragment(cita,listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos4).commit();
            }
            notifyDataSetChanged();
        }

        @Override
        public void onSwitchDetailPhotos(Cita cita, int indexEtapa, int position) {
            Log.d(LOG_TAG,"Haciendo el switch de fragments to Detail Gallery");
            mFragmentManager.beginTransaction().detach(mFragmentAtPos4)
                    .commit();

            if (mFragmentAtPos4 instanceof DetailCitaFragment) {
                Log.d(LOG_TAG,"Haciendo el switch de to Browser Gallery");
                mFragmentAtPos4 = new pictureBrowserFragment(cita,cita.getOrdenes().get(indexEtapa).getDocumentos(),position,listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos4).commit();

            } else { // Instance of NextFragment
                mFragmentAtPos4 = new DetailCitaFragment(cita,listener);
                mFragmentManager.beginTransaction().attach(mFragmentAtPos4).commit();
            }
            notifyDataSetChanged();
        }


    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0

                if (mFragmentAtPos0 == null) {
                    mFragmentAtPos0 = new MotosFragment(listener);
                }

                return mFragmentAtPos0;
            case 1: // Fragment # 1
                return new TalleresFragment();
            case 2:// Fragment # 2
                if(mFragmentAtPos3 == null){
                    mFragmentAtPos3 = new UserFragment(listener);
                }
                return mFragmentAtPos3;
            case 3:
                Log.d(LOG_TAG,"Es una instancia nueva");
                if (mFragmentAtPos4 == null) {
                    mFragmentAtPos4 = new CitasFragment(listener);
                }

                return mFragmentAtPos4;
        }
        return null;

    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public int getItemPosition(Object object) {
        Log.d(LOG_TAG," Obteniendo la psicion del objeto "+object.getClass().getCanonicalName());
        if (object instanceof MotosFragment &&
                mFragmentAtPos0 instanceof CreateVehiclesFragment) {
            return POSITION_NONE;
        }
        if (object instanceof CreateVehiclesFragment &&
                mFragmentAtPos0 instanceof MotosFragment) {
            return POSITION_NONE;
        }

        if (object instanceof MotosFragment &&
                mFragmentAtPos0 instanceof DocumentoVehiculoFragment) {
            return POSITION_NONE;
        }

        if (object instanceof DocumentoVehiculoFragment &&
                mFragmentAtPos0 instanceof MotosFragment) {
            return POSITION_NONE;
        }

        if (object instanceof UserFragment &&
                mFragmentAtPos3 instanceof UpdateUserFragment) {
            return POSITION_NONE;
        }
        if (object instanceof UpdateUserFragment &&
                mFragmentAtPos3 instanceof UserFragment) {
            return POSITION_NONE;
        }

        if (object instanceof CitasFragment &&
                mFragmentAtPos4 instanceof NewCitaFragment) {
            return POSITION_NONE;
        }
        if (object instanceof NewCitaFragment &&
                mFragmentAtPos4 instanceof CitasFragment) {
            return POSITION_NONE;
        }

        if (object instanceof CitasFragment &&
                mFragmentAtPos4 instanceof GalleryCitasFragment) {
            return POSITION_NONE;
        }

        if (object instanceof GalleryCitasFragment &&
                mFragmentAtPos4 instanceof CitasFragment) {
            return POSITION_NONE;
        }

        if (object instanceof DetailCitaFragment &&
                mFragmentAtPos4 instanceof CotizacionFragment) {
            return POSITION_NONE;
        }


        if (object instanceof CotizacionFragment &&
                mFragmentAtPos4 instanceof DetailCitaFragment) {
            return POSITION_NONE;
        }

        if (object instanceof DetailCitaFragment &&
                mFragmentAtPos4 instanceof pictureBrowserFragment) {
            return POSITION_NONE;
        }

        if (object instanceof pictureBrowserFragment &&
                mFragmentAtPos4 instanceof DetailCitaFragment) {
            return POSITION_NONE;
        }

        if (object instanceof CitasFragment &&
                mFragmentAtPos4 instanceof DetailCitaFragment) {
            return POSITION_NONE;
        }
        if (object instanceof DetailCitaFragment &&
                mFragmentAtPos4 instanceof CitasFragment) {
            return POSITION_NONE;
        }


        return POSITION_UNCHANGED;
    }



}


