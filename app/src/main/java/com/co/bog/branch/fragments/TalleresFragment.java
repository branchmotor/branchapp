package com.co.bog.branch.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.TalleresViewAdapter;
import com.co.bog.branch.ws.models.Taller;
import com.co.bog.branch.ws.models.Vehiculo;
import com.co.bog.branch.ws.service.BranchService;
import com.co.bog.branch.ws.ws;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TalleresFragment extends Fragment  {

    private static final String LOG_TAG = TalleresFragment.class.getSimpleName();

    private List<Taller> listTalleresCliente = new ArrayList<>();

    private RecyclerView recyclerViewTalleresCliente;

    private RecyclerView.Adapter mAdapter;

    private RecyclerView.LayoutManager mLayoutManager;

    private FirebaseAuth mAuth;

    public TalleresFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_talleres, container, false);

        mAuth = FirebaseAuth.getInstance();

        this.recyclerViewTalleresCliente = (RecyclerView) rootView.findViewById(R.id.talleresClienteRecycler);

        return rootView;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView(recyclerViewTalleresCliente);

    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {

        mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);

        fetchTalleres();

    }

    private void fetchTalleres() {

        BranchService service = ws.getService(getContext()).create(BranchService.class);

        FirebaseUser user = mAuth.getCurrentUser();

        Call<List<Vehiculo>> vehiculoCall = service.listVehiculos(user.getUid());

        vehiculoCall.enqueue(new Callback<List<Vehiculo>>() {
            @Override
            public void onResponse(Call<List<Vehiculo>> call, Response<List<Vehiculo>> response) {
                if(response.isSuccessful()){
                    List<Vehiculo> MylistVehiculos = response.body();
                    for(Vehiculo vehiculo: MylistVehiculos){
                        if(vehiculo.getTaller()!= null){
                            if(!listTalleresCliente.contains(vehiculo.getTaller())){
                                listTalleresCliente.add(vehiculo.getTaller());
                            }

                        }
                    }
                    Log.i(LOG_TAG,"Resultado lista talleres :::>"+listTalleresCliente.toString());
                    //mAdapter.notifyDataSetChanged();
                    //mAdapter.notifyAll();
                    mAdapter = new TalleresViewAdapter(listTalleresCliente, R.layout.taller_item, new TalleresViewAdapter.OnItemClickListener() {
                        @Override
                        public void OnItemClick(Taller taller, int position) {

                        }
                    });

                    recyclerViewTalleresCliente.setAdapter(mAdapter);
                }else{
                    int statusCode = response.code();
                    Log.e(LOG_TAG, "onResponse(): Error code = " + statusCode);
                }
            }

            @Override
            public void onFailure(Call<List<Vehiculo>> call, Throwable t) {
                //Toast.makeText(MotosFragment.this,"Error", Toast.LENGTH_SHORT).show();
                Log.e(LOG_TAG, "Error consumir WS = " + t.getMessage());
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Fragment fragment = (getFragmentManager().findFragmentById(R.id.mapBranch));
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.remove(fragment);
        ft.commit();
    }
}
