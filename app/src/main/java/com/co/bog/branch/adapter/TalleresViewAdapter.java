package com.co.bog.branch.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.co.bog.branch.R;
import com.co.bog.branch.ws.models.Taller;
import com.co.bog.branch.ws.models.Vehiculo;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class TalleresViewAdapter extends RecyclerView.Adapter<TalleresViewAdapter.ViewHolder> {

    private static final String LOG_TAG = TalleresViewAdapter.class.getSimpleName();

    private List<Taller> listTalleres;

    private int layout;

    private OnItemClickListener itemClickListener;


    public TalleresViewAdapter(List<Taller> listTalleres, int layout, OnItemClickListener itemClickListener) {
        this.listTalleres = listTalleres;
        this.layout = layout;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(this.listTalleres.get(position), itemClickListener);
    }

    @Override
    public int getItemCount() {
        return this.listTalleres.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textViewNameTaller;

        private TextView textViewTelefonoTaller;

        private TextView textViewDireccionTaller;

        private CircleImageView imagelogoTaller;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textViewNameTaller = (TextView) itemView.findViewById(R.id.textNombreTaller);
            this.textViewTelefonoTaller = (TextView) itemView.findViewById(R.id.textTelefonoTaller);
            this.textViewDireccionTaller = (TextView) itemView.findViewById(R.id.textDireccionTaller);
            this.imagelogoTaller = (CircleImageView) itemView.findViewById(R.id.imagelogoTaller);
        }

        public void bind(final Taller taller, final OnItemClickListener onItemClickListener) {

            Log.i(LOG_TAG, "Se va a reenderizar el taller :::>" + taller);
            this.textViewNameTaller.setText(taller.getNombre());
            this.textViewTelefonoTaller.setText(taller.getCelular());
            this.textViewDireccionTaller.setText(taller.getDireccion());

            if(taller.getLogo() != null){
                Picasso.get().load(taller.getLogo()).into(this.imagelogoTaller);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClick(taller, getAdapterPosition());
                }
            });
        }
    }

    public interface OnItemClickListener {
        void OnItemClick(Taller taller, int position);
    }
}
