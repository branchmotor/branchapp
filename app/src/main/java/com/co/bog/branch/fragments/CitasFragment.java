package com.co.bog.branch.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.PageAdapterCitas;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

/**
 * A simple {@link Fragment} subclass.
 */
public class CitasFragment extends Fragment implements FloatingActionButton.OnClickListener {

    private static final String LOG_TAG = CitasFragment.class.getSimpleName();

    private ViewPager2 viewPagerCitas;

    private TabLayout tabsCitas;

    private FloatingActionButton floatButtonNewCita;

    public static SwitchFragmentListener switchFragmentsCitasListener;

    public CitasFragment(){
        Log.d(LOG_TAG,"Constructor vacio CitasFragment :::");
    }

    public CitasFragment(SwitchFragmentListener listener) {
        Log.d(LOG_TAG,"Constructor CitasFragment :::");
        this.switchFragmentsCitasListener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(LOG_TAG, "Create CIta Fragment");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_citas, container, false);


       this.floatButtonNewCita = (FloatingActionButton) view.findViewById(R.id.floatButtonNewCita);

        this.floatButtonNewCita.setOnClickListener(this);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(LOG_TAG, "View Created CIta Fragment"+switchFragmentsCitasListener.toString());

        tabsCitas = (TabLayout) view.findViewById(R.id.tabsCitas);

        viewPagerCitas = (ViewPager2) view.findViewById(R.id.viewPagerCitas);

        PageAdapterCitas adapter = new PageAdapterCitas(getActivity(),switchFragmentsCitasListener);

        viewPagerCitas.setAdapter(adapter);

        new TabLayoutMediator(tabsCitas, viewPagerCitas, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch (position){
                    case 0:
                        tab.setText("Pasadas");
                        break;
                    case 1:
                        tab.setText("Activas");
                        break;
                    case 2:
                        tab.setText("Futuras");
                        break;
                }

            }
        }).attach();

        //tabsCitas.setupWithViewPager(viewPagerCitas);

       // super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onClick(View v) {
        switchFragmentsCitasListener.onSwitchCitaFragment();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
