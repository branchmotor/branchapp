package com.co.bog.branch.adapter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.co.bog.branch.adapter.listener.SwitchDetailGalleryFragmentListener;
import com.co.bog.branch.fragments.GalleryFragment;
import com.co.bog.branch.fragments.pictureBrowserFragment;
import com.co.bog.branch.ws.enums.OrdenEtapa;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Documento;

import java.util.ArrayList;
import java.util.List;

public class PageAdapterGallery extends FragmentStatePagerAdapter {

    private static final String LOG_TAG = PageAdapterGallery.class.getSimpleName();

    private Cita cita;

    private FragmentManager mFragmentManager;

    private Fragment currentFragment2;

    private Fragment currentFragment1;

    private Fragment currentFragment0;

    private SwitchDetailPhoto listener = new SwitchDetailPhoto();

    public PageAdapterGallery(FragmentManager fm, Cita cita){
        super(fm);
        this.cita = cita;
        this.mFragmentManager = fm;
    }

    public final class SwitchDetailPhoto implements SwitchDetailGalleryFragmentListener {

        @Override
        public void onSwitchDetailDiagnosticoPhotos(List<Documento> listDocumentos,int position) {
            Log.d(LOG_TAG,"Haciendo el switch de fragments to Detail Gallery");
            mFragmentManager.beginTransaction().detach(currentFragment1)
                    .commit();

            if (currentFragment1 instanceof GalleryFragment) {
                Log.d(LOG_TAG,"Haciendo el switch de to Browser Gallery");
                //currentFragment1 = new pictureBrowserFragment(listDocumentos,position);
                mFragmentManager.beginTransaction().attach(currentFragment1).commit();

            } else { // Instance of NextFragment
                currentFragment1 = new GalleryFragment(listDocumentos, OrdenEtapa.DIAGNOSTICO.getValue(),listener);
                mFragmentManager.beginTransaction().attach(currentFragment1).commit();
            }
            notifyDataSetChanged();
        }

        @Override
        public void onSwitchDetailReparacionPhotos(List<Documento> listDocumentos, int position) {
            Log.d(LOG_TAG,"Haciendo el switch de fragments to Detail Gallery");
            mFragmentManager.beginTransaction().detach(currentFragment2)
                    .commit();

            if (currentFragment1 instanceof GalleryFragment) {
                Log.d(LOG_TAG,"Haciendo el switch de to Browser Gallery");
                //currentFragment2 = new pictureBrowserFragment(listDocumentos,position);
                mFragmentManager.beginTransaction().attach(currentFragment2).commit();

            } else { // Instance of NextFragment
                currentFragment2 = new GalleryFragment(listDocumentos, OrdenEtapa.REPARACION.getValue(),listener);
                mFragmentManager.beginTransaction().attach(currentFragment2).commit();
            }
            notifyDataSetChanged();
        }
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Ingreso";
            case 1:
                return "Diagnostico";
            case 2:
                return "Reparación";
            default:
                return null;
        }
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Log.d(LOG_TAG,"Se va a reenderizar la posicion en Page Gallery :::>"+position);
        switch (position) {
            case 0:
                if (currentFragment0 == null) {
                    if(cita.getOrdenes().size() > 0) {
                        currentFragment0 = new GalleryFragment(cita.getOrdenes().get(0).getDocumentos(), cita.getOrdenes().get(0).getIdEtapa(), listener);
                    }else{
                        currentFragment0 = new GalleryFragment(new ArrayList<Documento>(),0,listener);
                    }
                }
                return currentFragment0;
            case 1:
                if (currentFragment1 == null) {
                    if (cita.getOrdenes().size() > 1){
                        currentFragment1 = new GalleryFragment(cita.getOrdenes().get(1).getDocumentos(), cita.getOrdenes().get(1).getIdEtapa(),listener);
                    }else{
                        currentFragment1 = new GalleryFragment(new ArrayList<Documento>(),0,listener);
                    }
                }
                return currentFragment1;
            case 2:
                if (currentFragment2 == null) {
                    if (cita.getOrdenes().size() > 3){
                        currentFragment2 = new GalleryFragment(cita.getOrdenes().get(4).getDocumentos(), cita.getOrdenes().get(4).getIdEtapa(),listener);
                    }else{
                        currentFragment2 = new GalleryFragment(new ArrayList<Documento>(),0,listener);
                    }
                }
                return currentFragment2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        if (object instanceof GalleryFragment &&
                currentFragment1 instanceof pictureBrowserFragment) {
            return POSITION_NONE;
        }
        if (object instanceof pictureBrowserFragment &&
                currentFragment1 instanceof GalleryFragment) {
            return POSITION_NONE;
        }

        if (object instanceof GalleryFragment &&
                currentFragment2 instanceof pictureBrowserFragment) {
            return POSITION_NONE;
        }
        if (object instanceof pictureBrowserFragment &&
                currentFragment2 instanceof GalleryFragment) {
            return POSITION_NONE;
        }

        return POSITION_UNCHANGED;
    }
}
