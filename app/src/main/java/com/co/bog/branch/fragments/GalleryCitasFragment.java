package com.co.bog.branch.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.PageAdapterGallery;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.co.bog.branch.ws.models.Cita;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryCitasFragment extends Fragment {

    private static final String LOG_TAG=GalleryCitasFragment.class.getSimpleName();

    private ViewPager viewPagerGalleryCitas;

    private TabLayout tabsEtapas;

    static SwitchFragmentListener switchFragmentsGalleryListener;

    private Cita cita;


    public GalleryCitasFragment(Cita cita, SwitchFragmentListener listener) {
        // Required empty public constructor
        this.cita = cita;
        this.switchFragmentsGalleryListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        try{
            // Inflate the layout for this fragment
            View view = inflater.inflate(R.layout.fragment_gallery_citas, container, false);

            tabsEtapas = (TabLayout) view.findViewById(R.id.tabsGallery);

            viewPagerGalleryCitas = (ViewPager) view.findViewById(R.id.viewPagerGallery);

            PageAdapterGallery adapter = new PageAdapterGallery(getActivity().getSupportFragmentManager(),cita);

            viewPagerGalleryCitas.setAdapter(adapter);

            tabsEtapas.setupWithViewPager(viewPagerGalleryCitas);

            return view;
        }catch (NullPointerException e){
            Log.e(LOG_TAG,"Null al reenderizar fragment de cita ::>" + e.getLocalizedMessage());
            return null;
        }



    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void backPressed() {
        switchFragmentsGalleryListener.onSwitchGalleryFragment(null);
    }
}
