package com.co.bog.branch.activities;

import java.util.*;

import android.app.*;
import android.content.*;
import android.content.pm.ActivityInfo;
import android.view.*;
import android.widget.*;

import android.os.*;

import com.co.bog.branch.R;
import com.google.firebase.auth.FirebaseAuth;

public class splashActivity extends Activity {
    //------------------DECLARANDO VARIALBES---------------------------//
    private Timer timer;
    private ProgressBar progressBar;
    private int i = 0;
    //---------------------FIN-----------------------------//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); */
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash);
//---------------------------------------SPLASH CON BARRA DE PROGRESO ------------------------------------------------//
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        progressBar.setProgress(0);
        final long intervalo = 45;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (i < 100){
                    progressBar.setProgress(i);
                    i++;
                }else{
                    timer.cancel();
                    Intent intentLogin = new Intent(getApplicationContext(), LoginActivity.class);
                    Intent intentMain = new Intent(getApplicationContext(), MainActivity.class);
                    FirebaseAuth auth = FirebaseAuth.getInstance();
                    if (auth.getCurrentUser() != null) {
                        startActivity(intentMain);
                    } else {
                        // not signed in
                        startActivity(intentLogin);
                    }
                    finish();
                }
            }
        },0,intervalo);
//--------------------------------------- FIN ------------------------------------------------//
    }
}


