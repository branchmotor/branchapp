package com.co.bog.branch.adapter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.co.bog.branch.adapter.listener.OnItemCitaListener;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.co.bog.branch.fragments.CitasListFragment;
import com.co.bog.branch.ws.enums.CitaEtapa;
import com.co.bog.branch.ws.models.Cita;

public class PageAdapterCitas extends FragmentStateAdapter implements OnItemCitaListener {

    private static final String LOG_TAG = PageAdapterCitas.class.getSimpleName();

    private SwitchFragmentListener listener;

    public PageAdapterCitas(FragmentActivity fm,  SwitchFragmentListener listener){
        super(fm);
        this.listener = listener;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Log.d(LOG_TAG,"Se va a reenderizar la posicion :::>"+position);
        switch (position) {
            case 0:
                CitasListFragment fragmentP = new CitasListFragment();

                fragmentP.setOnItemActiveListener(this);
                fragmentP.setTipoCitaListener(CitaEtapa.PASADA);

                return fragmentP;
            case 1:
                CitasListFragment fragmentA = new CitasListFragment();

                fragmentA.setOnItemActiveListener(this);
                fragmentA.setTipoCitaListener(CitaEtapa.ACTIVA);

                return fragmentA;
            case 2:
                CitasListFragment fragmentF = new CitasListFragment();

                fragmentF.setOnItemActiveListener(this);

                fragmentF.setTipoCitaListener(CitaEtapa.FUTURA);

                return fragmentF;
            default:
                return null;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }


    @Override
    public void viewDetailCita(Cita cita) {
        listener.onSwitchDetailCita(cita);
    }

    @Override
    public void goTovisualizadorPDF(Cita cita,int indexEtapaPdf) {
        listener.onSwitchVisualizadorPDF(cita);
    }


}
