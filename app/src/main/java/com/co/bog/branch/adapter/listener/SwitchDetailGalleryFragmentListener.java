package com.co.bog.branch.adapter.listener;

import androidx.fragment.app.Fragment;

import com.co.bog.branch.ws.models.Documento;

import java.util.List;

public interface SwitchDetailGalleryFragmentListener {

    void onSwitchDetailDiagnosticoPhotos(List<Documento> listDocumentos, int position);

    void onSwitchDetailReparacionPhotos(List<Documento> listDocumentos, int position);
}
