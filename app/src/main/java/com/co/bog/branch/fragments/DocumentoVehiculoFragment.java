package com.co.bog.branch.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.co.bog.branch.R;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.co.bog.branch.ws.models.Documento;
import com.co.bog.branch.ws.models.Vehiculo;
import com.co.bog.branch.ws.service.BranchService;
import com.co.bog.branch.ws.ws;
import com.google.android.material.card.MaterialCardView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class DocumentoVehiculoFragment extends Fragment implements View.OnClickListener{

    private static final String LOG_TAG = DocumentoVehiculoFragment.class.getSimpleName();

    private MaterialCardView frameTarjetPropiedad;

    private MaterialCardView frameSoat;

    private MaterialCardView frameTecnomecanica;

    private ImageView imageTecnomecanica;

    private ProgressBar progressBarSoat;

    private ImageView imageSoat;

    private SwitchFragmentListener switchUserListener;

    private Vehiculo vehiculo;

    private static final int REQUEST_IMAGE_GALERY_SOAT = 200;

    private static final int REQUEST_IMAGE_GALERY_TECNOMECANICA = 201;


    public DocumentoVehiculoFragment(){
        // Required empty public constructor
    }

    public DocumentoVehiculoFragment(SwitchFragmentListener switchUserListener, Vehiculo vehiculo) {

        this.switchUserListener = switchUserListener;
        this.vehiculo = vehiculo;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_doc_vehiculo, container, false);

        bindUI(view);

        return view;
    }

    private void bindUI(View view){

        this.frameTarjetPropiedad = (MaterialCardView) view.findViewById(R.id.layoutImageTarjetaPropiedad);

        this.frameSoat = (MaterialCardView) view.findViewById(R.id.layoutImageSoat);

        this.frameTecnomecanica = (MaterialCardView) view.findViewById(R.id.layoutImageTecnoMecanica);

        this.imageSoat = (ImageView) view.findViewById(R.id.photoSoat);

        this.progressBarSoat = (ProgressBar) view.findViewById(R.id.progressBarSoat);

        loadSoatImage();

        this.imageTecnomecanica = (ImageView) view.findViewById(R.id.photoTecnoMecanica);

        loadTecnoImage();

        this.frameTarjetPropiedad.setOnClickListener(this);

        if(vehiculo.getSoat()==null){
            this.frameSoat.setOnClickListener(this);
            this.progressBarSoat.setVisibility(View.VISIBLE);
        }else{
            if (this.progressBarSoat != null) {
                this.progressBarSoat.setVisibility(View.GONE);
            }
        }

        this.frameTecnomecanica.setOnClickListener(this);

    }

    private void loadSoatImage() {
        if(vehiculo.getSoat()!= null){
            Picasso.get().load(vehiculo.getSoat().getUrl()).into(this.imageSoat, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    Log.d(LOG_TAG,"Se cargo la photo en el image View de Soat");
                }

                @Override
                public void onError(Exception e) {

                }
            });
        }
    }


    private void loadTecnoImage() {
        if(vehiculo.getTecnoMecanica()!= null){
            Picasso.get().load(vehiculo.getTecnoMecanica().getUrl()).into(this.imageTecnomecanica, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    Log.d(LOG_TAG,"Se cargo la photo en el image View de Tecnomecanica");
                }

                @Override
                public void onError(Exception e) {

                }
            });
        }
    }

    private void startActivityLoadFiles(int layout){
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        switch (layout){
            case R.id.layoutImageTarjetaPropiedad:
                startActivityForResult(intent, REQUEST_IMAGE_GALERY_TECNOMECANICA);
                break;
            case R.id.layoutImageSoat:
                startActivityForResult(intent, REQUEST_IMAGE_GALERY_SOAT);
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE_GALERY_TECNOMECANICA && resultCode == RESULT_OK) {
            handleFileSelected(data, REQUEST_IMAGE_GALERY_TECNOMECANICA);
        }
        if (requestCode == REQUEST_IMAGE_GALERY_SOAT && resultCode == RESULT_OK) {
            handleFileSelected(data, REQUEST_IMAGE_GALERY_SOAT);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleFileSelected(@Nullable Intent data, int fileToUpload) {
        Uri selectedImage = data.getData();

        String[] filePath = { MediaStore.Images.Media.DATA };

        Log.d(LOG_TAG,"Path image load first File path "+filePath[0]);

        Cursor c = getActivity().getContentResolver().query(selectedImage,filePath, null, null, null);

        c.moveToFirst();

        int columnIndex = c.getColumnIndex(filePath[0]);

        String picturePath = c.getString(columnIndex);

        Log.d(LOG_TAG,"Path image load form Gallery "+picturePath);

        c.close();

        uploadToS3(new File(picturePath), fileToUpload);
    }

    /**
     *
     * @param file
     */
    private void uploadToS3(File file, int fileToUpload) {

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getActivity().getApplicationContext(),
                "us-east-2:2b36dfa2-1fe0-400a-82cb-cd167a14cc3a", // ID del grupo de identidades
                Regions.US_EAST_2 // Región
        );

        Log.d(LOG_TAG," File get name key to s3 :::>"+file.getName());

        AmazonS3 s3 = new AmazonS3Client(credentialsProvider);

        String extension = file.getName().substring(file.getName().lastIndexOf("."));

        String key = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())+extension;

        TransferUtility transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());
        //transferUtility.upload()
        final TransferObserver observer = transferUtility.upload(
                "branchmedia",  //this is the bucket name on S3
                key, //this is the path and name
                //file.getName(),
                file, //path to the file locally
                CannedAccessControlList.PublicRead //to make the file public
        );

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.equals(TransferState.COMPLETED)) {
                    //Success
                    try {

                        Log.d(LOG_TAG,"Se subio el archivo exitosamente to S3 ::>"+state.name());

                        String url = "https://branchmedia.s3.us-east-2.amazonaws.com/"+key;

                        Documento foto = new Documento();

                        foto.setUrl(url);

                        Calendar now = Calendar.getInstance();

                        foto.setDate(now.getTime().toString());

                        foto.setSize((int) file.length());

                        URLConnection connection = file.toURL().openConnection();

                        String mimeType = connection.getContentType();

                        foto.setType(mimeType);

                        foto.setNombreArchivo(file.getName());

                        foto.setKeynameFile(key);

                        foto.setValidated(false);

                        BranchService service = ws.getService(getContext()).create(BranchService.class);

                        if(fileToUpload == REQUEST_IMAGE_GALERY_SOAT){
                            vehiculo.setSoat(foto);
                        }

                        if(fileToUpload == REQUEST_IMAGE_GALERY_TECNOMECANICA){
                            vehiculo.setTecnoMecanica(foto);
                        }

                        Call<Object> vehiculoCall = service.updateVehiculo(vehiculo.getIdVehiculo(), vehiculo);

                        vehiculoCall.enqueue(new Callback<Object>() {
                            @Override
                            public void onResponse(Call<Object> call, Response<Object> response) {
                                if (response.isSuccessful()) {
                                    Log.d(LOG_TAG,"Se actualizo correctamente los documentos al vehiculo");
                                    loadSoatImage();
                                    loadTecnoImage();
                                } else {
                                    int statusCode = response.code();
                                    Log.e(LOG_TAG, "onResponse(): Respuesta fallida al actualizar fotos = " + statusCode);
                                }
                            }

                            @Override
                            public void onFailure(Call<Object> call, Throwable t) {
                                //Toast.makeText(MotosFragment.this,"Error", Toast.LENGTH_SHORT).show();
                                Log.e(LOG_TAG, "Error al actualizar las fotos WS = " + t.getMessage());
                                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });

                    }catch (MalformedURLException e){
                        Log.e(LOG_TAG,"Error MalFormed URL "+e.getMessage());
                    }catch (IOException e){
                        Log.e(LOG_TAG,"Error IO File "+e.getMessage());
                    }


                } else if (state.equals(TransferState.FAILED)) {
                    //Failed
                    Log.d(LOG_TAG,"Error al subir archivo to S3");
                }

            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                Log.d(LOG_TAG,"Progress Changed, bytesCurrent ::> "+bytesCurrent+ " Total ::>"+bytesTotal);
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.d(LOG_TAG,"Error en S3 "+ex.getMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layoutImageTarjetaPropiedad:
                startActivityLoadFiles(R.id.layoutImageTarjetaPropiedad);
                break;
            case R.id.layoutImageSoat:
                startActivityLoadFiles(R.id.layoutImageSoat);
                break;
        }
    }

    public void backPressed() {
        switchUserListener.onSwitchVehicleDocumentosFragment(null);
    }
}
