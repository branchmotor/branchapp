package com.co.bog.branch.ws.service;

import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Marca;
import com.co.bog.branch.ws.models.Orden;
import com.co.bog.branch.ws.models.Usuario;
import com.co.bog.branch.ws.models.Vehiculo;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface BranchService {

    @PUT("usuario/update/{uid}")
    Call<Object> updateUsuario(@Path("uid") String uid, @Body JsonObject usuario);

    @PUT("usuario/update/{uid}")
    Call<Object> updateUsuario(@Path("uid") String uid, @Body Usuario usuario);

    @GET("usuario/getByEmail/{email}")
    Call<Usuario> findUsuarioByEmail(@Path("email") String email);

    @POST("usuario/createFireBaseUser")
    Call<Usuario> crearUsuario(@Body Usuario usuario);

    @GET("vehiculo/getByIdUsuario/{Id}")
    Call<List<Vehiculo>> listVehiculos(@Path("Id") String IdUsuario);

    @POST("vehiculo/create")
    Call<Vehiculo> crearVehiculo(@Body Vehiculo vehiculo);

    @GET("vehiculo/getByPlaca/{placa}")
    Call<Vehiculo> getVehiculoByPlaca(@Path("placa") String placa);

    @PUT("vehiculo/update/{Id}")
    Call<Object> updateVehiculo(@Path("Id") Integer IdVehiculo,@Body Vehiculo vehiculo);

    @GET("cita/getByIdUsuario/{Id}")
    Call<List<Cita>> listCitas(@Path("Id") String IdUsuario);

    @POST("cita/create")
    Call<Cita> crearCita(@Body Cita cita);

    @GET("cita/getPasadasByIdUsuario/{Id}")
    Call<List<Cita>> listCitasPasadas(@Path("Id") String IdUsuario);

    @GET("cita/getActivasByIdUsuario/{Id}")
    Call<List<Cita>> listCitasActivas(@Path("Id") String IdUsuario);

    @GET("cita/getFuturasByIdUsuario/{Id}")
    Call<List<Cita>> listCitasFuturas(@Path("Id") String IdUsuario);

    @GET("marca/getAllUnique")
    Call<List<Marca>> listUniqueMarcas();

    @GET("marca/getAllByMarca/{marca}")
    Call<List<Marca>> listMarcas(@Path("marca") String marca);

    @PUT("orden/update/{Id}")
    Call<Object> updateOrden(@Path("Id") Integer IdOrdenTrabajo,@Body Orden orden);

    @POST("orden/create")
    Call<Orden> crearOrden(@Body Orden orden);
}
