package com.co.bog.branch.ws.deserializer;

import android.util.Log;

import com.co.bog.branch.util.Util;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Documento;
import com.co.bog.branch.ws.models.Mecanico;
import com.co.bog.branch.ws.models.Orden;
import com.co.bog.branch.ws.models.Taller;
import com.co.bog.branch.ws.models.Vehiculo;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class DeserializerCita implements JsonDeserializer<Cita> {

    private static final String LOG_TAG = DeserializerCita.class.getSimpleName();

    @Override
    public Cita deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
        try {
            Cita cita = new Cita();

            cita.setIdCita(json.getAsJsonObject().get("IdCita").getAsInt());

            if (!json.getAsJsonObject().get("estado").isJsonNull()) {
                cita.setEstado(json.getAsJsonObject().get("estado").getAsString());
            }

            if (!json.getAsJsonObject().get("servicio").isJsonNull()) {
                cita.setServicio(json.getAsJsonObject().get("servicio").getAsString());
            }

            cita.setFechaCita(json.getAsJsonObject().get("fechaCita").getAsString().substring(0, 10) + " " + json.getAsJsonObject().get("horaCita").getAsString());


            if (json.getAsJsonObject().get("vehiculo") != null && !json.getAsJsonObject().get("vehiculo").isJsonNull()) {
                Vehiculo vehiculo = new Vehiculo();

                vehiculo.setIdVehiculo(json.getAsJsonObject().get("vehiculo").getAsJsonObject().get("IdVehiculo").getAsInt());
                vehiculo.setIdUsuario(json.getAsJsonObject().get("vehiculo").getAsJsonObject().get("IdUsuario").getAsString());
                vehiculo.setTipoVehiculo(json.getAsJsonObject().get("vehiculo").getAsJsonObject().get("tipoVehiculo").getAsString());
                vehiculo.setPlaca(json.getAsJsonObject().get("vehiculo").getAsJsonObject().get("placa").getAsString());
                vehiculo.setEstado(json.getAsJsonObject().get("vehiculo").getAsJsonObject().get("estado").getAsString());

                cita.setVehiculo(vehiculo);
            }

            if (json.getAsJsonObject().get("taller") != null && !json.getAsJsonObject().get("taller").isJsonNull()) {
                Taller taller = new Taller();

                taller.setIdTaller(json.getAsJsonObject().get("taller").getAsJsonObject().get("IdTaller").getAsInt());
                taller.setNombre(json.getAsJsonObject().get("taller").getAsJsonObject().get("nombre").getAsString());
                taller.setIdentificacion(json.getAsJsonObject().get("taller").getAsJsonObject().get("identificacion").getAsString());
                taller.setDireccion(json.getAsJsonObject().get("taller").getAsJsonObject().get("direccion").getAsString());
                taller.setCelular(json.getAsJsonObject().get("taller").getAsJsonObject().get("celular").getAsString());
                taller.setEmail(json.getAsJsonObject().get("taller").getAsJsonObject().get("email").getAsString());
                taller.setLogo(json.getAsJsonObject().get("taller").getAsJsonObject().get("logo").getAsString());
                taller.setEstado(json.getAsJsonObject().get("taller").getAsJsonObject().get("estado").getAsString());

                cita.setObjTaller(taller);
            }

            if (json.getAsJsonObject().get("ordentrabajos") != null && !json.getAsJsonObject().get("ordentrabajos").isJsonNull()) {
                JsonArray arrayOrdenes = json.getAsJsonObject().get("ordentrabajos").getAsJsonArray();
                for (JsonElement obj : arrayOrdenes) {
                    Orden orden = new Orden();
                    orden.setIdOrdenTrabajo(obj.getAsJsonObject().get("IdOrdenTrabajo").getAsInt());
                    orden.setIdTaller(obj.getAsJsonObject().get("IdTaller").getAsInt());
                    orden.setCodigoOrden(obj.getAsJsonObject().get("CodigoOrden").getAsString());
                    orden.setIdEtapa(obj.getAsJsonObject().get("IdEtapa").getAsInt());
                    orden.setIdCita(obj.getAsJsonObject().get("IdCita").getAsInt());
                    orden.setIdMecanico(obj.getAsJsonObject().get("IdMecanico").getAsInt());
                    orden.setIdVehiculo(obj.getAsJsonObject().get("IdVehiculo").getAsInt());

                    if (!obj.getAsJsonObject().get("kilometraje").isJsonNull()) {
                        orden.setKilometraje(obj.getAsJsonObject().get("kilometraje").getAsInt());
                    }

                    if (!obj.getAsJsonObject().get("estado").isJsonNull()) {
                        orden.setEstado(obj.getAsJsonObject().get("estado").getAsString());
                    }

                    if (!obj.getAsJsonObject().get("documentos").isJsonNull()) {
                        for(JsonElement element: obj.getAsJsonObject().get("documentos").getAsJsonArray()){
                            Documento documento = new Documento();

                            documento.setUrl(element.getAsJsonObject().get("url").getAsString());
                            documento.setUrlThumbnail(Util.generateThumbnailUrl(element.getAsJsonObject().get("url").getAsString()));
                            documento.setKeynameFile(Util.getKeyFileFromURL(element.getAsJsonObject().get("url").getAsString()));

                            orden.addDocumento(documento);
                        }
                    }

                    if (obj.getAsJsonObject().get("mecanico") != null && !obj.getAsJsonObject().get("mecanico").isJsonNull()) {
                        Mecanico mecanico = new Mecanico();

                        mecanico.setIdMecanico(obj.getAsJsonObject().get("mecanico").getAsJsonObject().get("IdMecanico").getAsInt());
                        mecanico.setFullName(obj.getAsJsonObject().get("mecanico").getAsJsonObject().get("fullName").getAsString());
                        mecanico.setIdentificacion(obj.getAsJsonObject().get("mecanico").getAsJsonObject().get("identificacion").getAsString());

                        orden.setMecanico(mecanico);
                    }

                    if (!obj.getAsJsonObject().get("Observaciones").isJsonNull()) {
                        orden.setObservaciones(obj.getAsJsonObject().get("Observaciones").getAsString());
                    }

                    try {

                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

                        orden.setCreatedAt(format.parse(obj.getAsJsonObject().get("createdAt").getAsString()));

                    } catch (ParseException e) {
                        Log.d(LOG_TAG, "Error al formatear fecha de la cita :::>" + e.getMessage());
                    }

                    //Log.d(LOG_TAG,"Orden a agregar a la cita :::>"+orden.toString());

                    cita.addOrden(orden);
                }
            }

            return cita;

        } catch (JsonParseException e) {
            Log.e(LOG_TAG, "Error al parcear Cita error ::>" + e.getLocalizedMessage() + " Error message ::>" + e.getMessage());
            return null;
        }
    }
}
