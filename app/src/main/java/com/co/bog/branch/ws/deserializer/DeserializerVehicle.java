package com.co.bog.branch.ws.deserializer;

import android.util.Log;

import com.co.bog.branch.util.Util;
import com.co.bog.branch.ws.models.Documento;
import com.co.bog.branch.ws.models.Marca;
import com.co.bog.branch.ws.models.Taller;
import com.co.bog.branch.ws.models.Usuario;
import com.co.bog.branch.ws.models.Vehiculo;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DeserializerVehicle implements JsonDeserializer<Vehiculo> {

    private static final String LOG_TAG = DeserializerVehicle.class.getSimpleName();

    @Override
    public Vehiculo deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        Vehiculo vehiculo = new Vehiculo();

        vehiculo.setIdVehiculo(json.getAsJsonObject().get("IdVehiculo").getAsInt());
        vehiculo.setTipoVehiculo(json.getAsJsonObject().get("tipoVehiculo").getAsString());
        vehiculo.setPlaca(json.getAsJsonObject().get("placa").getAsString());

        if(json.getAsJsonObject().get("IdTaller") != null && !json.getAsJsonObject().get("IdTaller").isJsonNull()){
            vehiculo.setIdTaller(json.getAsJsonObject().get("IdTaller").getAsInt());
        }

        if(!json.getAsJsonObject().get("kilometraje").isJsonNull()){
            vehiculo.setKilometraje(json.getAsJsonObject().get("kilometraje").getAsInt());
        }

        if(!json.getAsJsonObject().get("modelo").isJsonNull()){
            vehiculo.setModelo(json.getAsJsonObject().get("modelo").getAsInt());
        }

        if(!json.getAsJsonObject().get("color").isJsonNull()){
            vehiculo.setColor(json.getAsJsonObject().get("color").getAsString());
        }

        if(!json.getAsJsonObject().get("alias").isJsonNull()){
            vehiculo.setAlias(json.getAsJsonObject().get("alias").getAsString());
        }

        if(json.getAsJsonObject().get("fotos") != null && !json.getAsJsonObject().get("fotos").isJsonNull()){
            for(JsonElement element: json.getAsJsonObject().get("fotos").getAsJsonArray()){
                Documento documento = new Documento();

                documento.setUrl(element.getAsJsonObject().get("url").getAsString());
                documento.setUrlThumbnail(Util.generateThumbnailUrl(element.getAsJsonObject().get("url").getAsString()));
                documento.setKeynameFile(Util.getKeyFileFromURL(element.getAsJsonObject().get("url").getAsString()));

                vehiculo.addPhoto(documento);
            }
            //vehiculo.setFotos(json.getAsJsonObject().get("fotos").getAsJsonArray());
        }

        if(json.getAsJsonObject().get("soat") != null && !json.getAsJsonObject().get("soat").isJsonNull()){
            JsonElement documento = json.getAsJsonObject().get("soat").getAsJsonObject();

            Documento soat = new Documento();

            soat.setUrl(documento.getAsJsonObject().get("url").getAsString());
            soat.setUrlThumbnail(Util.generateThumbnailUrl(documento.getAsJsonObject().get("url").getAsString()));
            soat.setKeynameFile(Util.getKeyFileFromURL(documento.getAsJsonObject().get("url").getAsString()));
            soat.setValidated(documento.getAsJsonObject().get("validated").getAsBoolean());

            vehiculo.setSoat(soat);
        }

        if(!json.getAsJsonObject().get("fechaCompra").isJsonNull()){
            try {
                Log.d(LOG_TAG, "Fecha compra valor :::>" + json.getAsJsonObject().get("fechaCompra").getAsString());
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                vehiculo.setFechaCompra(format.parse(json.getAsJsonObject().get("fechaCompra").getAsString()));
            }catch (ParseException e){
                Log.e(LOG_TAG, "Error al formatear fecha de compra a fecha :::>"+e.getMessage());
            }
            //vehiculo.setFechaCompra(json.getAsJsonObject().get("fechaCompra").getAsString());
        }


        vehiculo.setEstado(json.getAsJsonObject().get("estado").getAsString());

        if(json.getAsJsonObject().get("marca") != null && !json.getAsJsonObject().get("marca").isJsonNull()){

            Marca marca = new Marca();

            marca.setIdMarca(json.getAsJsonObject().get("marca").getAsJsonObject().get("IdMarca").getAsInt());
            marca.setMarca(json.getAsJsonObject().get("marca").getAsJsonObject().get("marca").getAsString());
            marca.setReferencia(json.getAsJsonObject().get("marca").getAsJsonObject().get("referencia").getAsString());
            marca.setCategoria(json.getAsJsonObject().get("marca").getAsJsonObject().get("categoria").getAsString());
            marca.setTipo(json.getAsJsonObject().get("marca").getAsJsonObject().get("tipo").getAsString());
            marca.setDescripcion(json.getAsJsonObject().get("marca").getAsJsonObject().get("descripcion").getAsString());
            marca.setTipoVehiculo(json.getAsJsonObject().get("marca").getAsJsonObject().get("tipoVehiculo").getAsString());
            marca.setUrllogo(json.getAsJsonObject().get("marca").getAsJsonObject().get("urllogo").getAsString());

            vehiculo.setMarca(marca);

        }

        if(json.getAsJsonObject().get("usuario") != null && !json.getAsJsonObject().get("usuario").isJsonNull()){
            Usuario user = new Usuario();

            user.setIdUsuario(json.getAsJsonObject().get("usuario").getAsJsonObject().get("IdUsuario").getAsInt());
            user.setFirstName(json.getAsJsonObject().get("usuario").getAsJsonObject().get("firstName").getAsString());
            user.setEmail(json.getAsJsonObject().get("usuario").getAsJsonObject().get("email").getAsString());
            user.setUid(json.getAsJsonObject().get("usuario").getAsJsonObject().get("uid").getAsString());

            if(!json.getAsJsonObject().get("usuario").getAsJsonObject().get("celular").isJsonNull()){
                user.setCelular(json.getAsJsonObject().get("usuario").getAsJsonObject().get("celular").getAsString());
            }
            user.setTipoUsuario(json.getAsJsonObject().get("usuario").getAsJsonObject().get("tipoUsuario").getAsString());
            user.setEstado(json.getAsJsonObject().get("usuario").getAsJsonObject().get("estado").getAsString());

            vehiculo.setUsuario(user);
        }

        if(json.getAsJsonObject().get("taller") != null && !json.getAsJsonObject().get("taller").isJsonNull()){
            Taller taller = new Taller();

            taller.setIdTaller(json.getAsJsonObject().get("taller").getAsJsonObject().get("IdTaller").getAsInt());
            taller.setNombre(json.getAsJsonObject().get("taller").getAsJsonObject().get("nombre").getAsString());
            taller.setIdentificacion(json.getAsJsonObject().get("taller").getAsJsonObject().get("identificacion").getAsString());
            taller.setDireccion(json.getAsJsonObject().get("taller").getAsJsonObject().get("direccion").getAsString());
            taller.setCelular(json.getAsJsonObject().get("taller").getAsJsonObject().get("celular").getAsString());
            taller.setEmail(json.getAsJsonObject().get("taller").getAsJsonObject().get("email").getAsString());
            taller.setLogo(json.getAsJsonObject().get("taller").getAsJsonObject().get("logo").getAsString());
            taller.setEstado(json.getAsJsonObject().get("taller").getAsJsonObject().get("estado").getAsString());

            vehiculo.setTaller(taller);
        }



        return vehiculo;
    }
}
