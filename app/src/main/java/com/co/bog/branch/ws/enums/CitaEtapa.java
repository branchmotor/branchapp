package com.co.bog.branch.ws.enums;

public enum CitaEtapa {

    PASADA("Pasada", "Pasada"),ACTIVA("Activa", "Activa"), FUTURA("Futura","Futura");

    private final String key;
    private final String value;

    CitaEtapa(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }
    public String getValue() {
        return value;
    }
}
