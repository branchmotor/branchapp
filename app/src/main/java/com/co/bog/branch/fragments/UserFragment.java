package com.co.bog.branch.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.co.bog.branch.R;
import com.co.bog.branch.activities.LoginActivity;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

public class UserFragment extends Fragment implements View.OnClickListener {

    private static final String LOG_TAG = UserFragment.class.getSimpleName();

    private FirebaseAuth mAuth;

    private FirebaseUser currentUser;

    private ImageView imagenProfile;

    private TextView textNameUser;

    private MaterialButton btnEditarUsuario;

    private MaterialButton btnCerrarSession;

    public static SwitchFragmentListener switchFragmentsUserListener;


    public UserFragment(SwitchFragmentListener switchFragmentsUserListener) {
        // Required empty public constructor
        this.switchFragmentsUserListener = switchFragmentsUserListener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mAuth = FirebaseAuth.getInstance();

        currentUser = mAuth.getCurrentUser();

        Log.d(LOG_TAG, "userLoged: " + currentUser.toString());

        View view = inflater.inflate(R.layout.fragment_user, container, false);

        bindUI(view);

        return view;
    }


    private void bindUI(@NotNull View view) {

        imagenProfile = (ImageView) view.findViewById(R.id.userPhoto);

        Picasso.get().load(R.drawable.bg_sin_logo).fit().into(imagenProfile);

        textNameUser = (TextView) view.findViewById(R.id.textNameUser);
        textNameUser.setText(currentUser.getDisplayName());

        btnEditarUsuario = (MaterialButton) view.findViewById(R.id.buttonEditarUsuario);
        this.btnCerrarSession = (MaterialButton) view.findViewById(R.id.buttonLogout);

        btnEditarUsuario.setOnClickListener(this);
        this.btnCerrarSession.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonEditarUsuario:
                switchFragmentsUserListener.onSwitchUserFragment();
                break;
            case R.id.buttonLogout:
                logOut();
                //switchFragmentsUserListener.onSwitchUserFragment();
                break;
        }
    }

    private void logOut() {
        Log.d(LOG_TAG,"Button LogOut pressed");
        mAuth.signOut();
        Intent intent = new Intent( getContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void backPressed() {
        switchFragmentsUserListener.onSwitchVehicleDocumentosFragment(null);
    }
}
