package com.co.bog.branch.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.ImagesPagerAdapter;
import com.co.bog.branch.adapter.listener.SwitchFragmentListener;
import com.co.bog.branch.adapter.recyclerViewPagerImageAdapter;
import com.co.bog.branch.app.CustomProgress;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Documento;

import java.util.ArrayList;
import java.util.List;

import static androidx.core.view.ViewCompat.setTransitionName;

/**
 * A simple {@link Fragment} subclass.
 */
public class pictureBrowserFragment extends Fragment {

    private static final String LOG_TAG = pictureBrowserFragment.class.getSimpleName();

    private List<Documento> allImages = new ArrayList<>();
    private int position;
    private ImageView image;
    private ViewPager imagePager;
    private RecyclerView indicatorRecycler;
    private RecyclerView.Adapter indicatorAdapter;

    CustomProgress customProgress = CustomProgress.getInstance();

    private SwitchFragmentListener switchFragmentListener;

    private Cita cita;

    //private int viewVisibilityController;
    //private int viewVisibilitylooper;
    private int previousSelected = -1;

    public pictureBrowserFragment() {
        // Required empty public constructor
    }

    public pictureBrowserFragment(Cita cita, List<Documento> allImages, int imagePosition, SwitchFragmentListener switchFragmentListener) {
        this.cita = cita;
        this.allImages = allImages;
        this.position = imagePosition;
        this.switchFragmentListener = switchFragmentListener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        customProgress.showProgress(getContext(), R.string.lblEsperar, false);
        return inflater.inflate(R.layout.fragment_picture_browser, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /**
         * initialisation of the recyclerView visibility control integers
         */
        //viewVisibilityController = 0;
        //viewVisibilitylooper = 0;




        /**
         * setting up the recycler view indicator for the viewPager
         */
        indicatorRecycler = (RecyclerView) view.findViewById(R.id.indicatorRecycler);
        indicatorRecycler.hasFixedSize();
        indicatorRecycler.setLayoutManager(new GridLayoutManager(getContext(),1,RecyclerView.HORIZONTAL,false));

        ImagesPagerAdapter pagingImages = new ImagesPagerAdapter(image,indicatorRecycler,allImages);

        /**
         * setting up the viewPager with images
         */
        imagePager = (ViewPager) view.findViewById(R.id.imagePager);

        imagePager.setAdapter(pagingImages);
        imagePager.setOffscreenPageLimit(3);

        Log.d(LOG_TAG," Current position to Set image Pager :::>"+position);

        imagePager.setCurrentItem(position);//displaying the image at the current position passed by the ImageDisplay Activity

        indicatorAdapter = new recyclerViewPagerImageAdapter(allImages, new recyclerViewPagerImageAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int ImagePosition) {
                //the below lines of code highlights the currently select image in  the indicatorRecycler with respect to the viewPager position
                Log.d(LOG_TAG,"Image previous selected :::> "+previousSelected+ " Current Image selected ::::> "+ImagePosition);
                if(previousSelected != -1){
                    allImages.get(previousSelected).setSelected(false);
                    previousSelected = ImagePosition;
                    //allImages.get(ImagePosition).setSelected(true);
                    indicatorRecycler.getAdapter().notifyDataSetChanged();
                    //indicatorRecycler.scrollToPosition(ImagePosition);
                }else{
                    previousSelected = ImagePosition;

                }

                imagePager.setCurrentItem(ImagePosition);
            }
        });


        indicatorRecycler.setAdapter(indicatorAdapter);

        //adjusting the recyclerView indicator to the current position of the viewPager, also highlights the image in recyclerView with respect to the
        //viewPager's position
        allImages.get(position).setSelected(true);
        previousSelected = position;
        indicatorAdapter.notifyDataSetChanged();
        indicatorRecycler.scrollToPosition(position);



        /**
         * this listener controls the visibility of the recyclerView
         * indication and it current position in respect to the image ViewPager
         */
        imagePager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if(previousSelected != -1){
                    allImages.get(previousSelected).setSelected(false);
                    previousSelected = position;
                    allImages.get(position).setSelected(true);
                    indicatorRecycler.getAdapter().notifyDataSetChanged();
                    indicatorRecycler.scrollToPosition(position);
                }else{
                    previousSelected = position;
                    allImages.get(position).setSelected(true);
                    indicatorRecycler.getAdapter().notifyDataSetChanged();
                    indicatorRecycler.scrollToPosition(position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        indicatorRecycler.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                /**
                 *  uncomment the below condition to control recyclerView visibility automatically
                 *  when image is clicked also uncomment the condition set on the image's onClickListener in the ImagesPagerAdapter adapter
                 */
                /*if(viewVisibilityController == 0){
                    indicatorRecycler.setVisibility(View.VISIBLE);
                    visibiling();
                }else{
                    viewVisibilitylooper++;
                }*/
                return false;
            }
        });

        customProgress.hideProgress();
    }



    public void backPressed() {
        switchFragmentListener.onSwitchDetailPhotos(cita,0,0);
    }

}
