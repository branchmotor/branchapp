package com.co.bog.branch.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.CitasViewAdapter;
import com.co.bog.branch.adapter.listener.OnItemCitaListener;
import com.co.bog.branch.adapter.listener.OnItemClickListener;
import com.co.bog.branch.app.CustomProgress;
import com.co.bog.branch.ws.enums.CitaEtapa;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.service.BranchService;
import com.co.bog.branch.ws.ws;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CitasListFragment extends Fragment {

    private static final String LOG_TAG = CitasListFragment.class.getSimpleName();

    private List<Cita> listCitas = new ArrayList<>();

    private RecyclerView recyclerViewCitas;

    private RecyclerView.Adapter mAdapter;

    private RecyclerView.LayoutManager mLayoutManager;

    private TextView emptyView;

    private FirebaseAuth mAuth;

    private OnItemCitaListener listener;

    private CitaEtapa citaEtapa;

    private View view;


    public CitasListFragment() {
        // Required empty public constructor
    }

    public void setOnItemActiveListener(OnItemCitaListener listener) {
        Log.d(LOG_TAG, "Set listener en fragment");
        this.listener = listener;
    }

    public void setTipoCitaListener(CitaEtapa citaEtapa){
        this.citaEtapa = citaEtapa;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_citas_list, container, false);

        mAuth = FirebaseAuth.getInstance();

        this.recyclerViewCitas = (RecyclerView) view.findViewById(R.id.citasRecycler);

        this.emptyView = (TextView) view.findViewById(R.id.empty_view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setupRecyclerView(this.recyclerViewCitas);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {

        mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);

        fetchCitas();

    }

    private void fetchCitas() {

        BranchService service = ws.getService(getContext()).create(BranchService.class);

        FirebaseUser user = mAuth.getCurrentUser();

        //CustomProgress customProgress = CustomProgress.getInstance();

        //customProgress.showProgress(getContext(), R.string.lblEsperar, false);

        Call<List<Cita>> citaCall;

        switch(citaEtapa){
            case PASADA:
                citaCall = service.listCitasPasadas(user.getUid());
                break;
            case ACTIVA:
                citaCall = service.listCitasActivas(user.getUid());
                break;
            case FUTURA:
                citaCall = service.listCitasFuturas(user.getUid());
                break;
            default:
                citaCall = service.listCitasPasadas(user.getUid());
        }


        citaCall.enqueue(new Callback<List<Cita>>() {
            @Override
            public void onResponse(Call<List<Cita>> call, Response<List<Cita>> response) {
                if(response.isSuccessful()){
                    listCitas = response.body();

                    //customProgress.hideProgress();

                    Log.i(LOG_TAG,"Resultado listar citas :::>"+listCitas.toString());

                    mAdapter = new CitasViewAdapter(listCitas, R.layout.cita_item, new OnItemClickListener() {
                        @Override
                        public void OnItemClick(Cita cita, int position, int layout) {
                            listener.viewDetailCita(cita);
                        }
                    });

                    recyclerViewCitas.setAdapter(mAdapter);

                    if (listCitas.isEmpty()) {
                        recyclerViewCitas.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                    }
                    else {
                        recyclerViewCitas.setVisibility(View.VISIBLE);
                        emptyView.setVisibility(View.GONE);
                    }



                }else{
                    //customProgress.hideProgress();
                    int statusCode = response.code();
                    Log.e(LOG_TAG, "onResponse(): Codigo respuesta Citas Pasadas Error code = " + statusCode);
                    Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<List<Cita>> call, Throwable t) {
                //Toast.makeText(MotosFragment.this,"Error", Toast.LENGTH_SHORT).show();
                Log.e(LOG_TAG, "Error listar citas WS = " + t.getMessage());
                Snackbar.make(view, R.string.msgError, Snackbar.LENGTH_LONG)
                        .show();
            }
        });
    }


}
