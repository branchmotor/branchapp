package com.co.bog.branch.adapter.listener;

import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Orden;

public interface OnItemCitaListener {

    void viewDetailCita(Cita cita);

    void goTovisualizadorPDF(Cita cita,int indexEtapaPdf);


}
