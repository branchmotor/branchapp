package com.co.bog.branch.fragments;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.co.bog.branch.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapTalleresFragment extends Fragment implements OnMapReadyCallback, LocationListener {

    private static final String LOG_TAG = MapTalleresFragment.class.getSimpleName();

    private final int LOCATION_PERMISSION_CODE = 100;

    private View rootView;

    private MapView mapView;

    private GoogleMap map;

    private LocationManager locationManager;

    private Marker marker;

    private CameraPosition cameraZoom;


    public MapTalleresFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_map_talleres, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapView = (MapView) rootView.findViewById(R.id.mapViewTalleres);

        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(LOG_TAG," Se recibio una actualizacion de posicion ::>"+location.getLatitude()+" , "+location.getLongitude());
        //Toast.makeText(getContext(), "Changed", Toast.LENGTH_LONG).show();
        createOrUpdateMarkerByLocation(location);
        zoomToLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    private void zoomToLocation(Location location) {
        cameraZoom = CameraPosition.builder().target(new LatLng(location.getLatitude(), location.getLongitude())).zoom(15).bearing(0).tilt(30).build();
        this.map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraZoom));
    }

    private void createOrUpdateMarkerByLocation(Location location) {
        if (marker == null) {
            Log.d(LOG_TAG," se va a crear una nueva instancia del marcador a la ubicacion actual :::> "+location.getLatitude()+" , "+location.getLongitude());
            marker = this.map.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())));
        } else {
            Log.d(LOG_TAG," se va a actulizar la instancia del marcador a la posicion ::>"+location.getLatitude()+" , "+location.getLongitude());
            marker.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        //Compr
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(checkPermission(Manifest.permission.ACCESS_FINE_LOCATION) && checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION)  ){
                // Ha aceptado
                updateLocation();

            }else {
                Log.d(LOG_TAG," No tiene permisos los va a solicitar");

                if (!shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Log.d(LOG_TAG," Access fine location request permission before");
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_PERMISSION_CODE);
                }
                else {
                    Log.d(LOG_TAG," Ya denego los permisos los va a solicitar");
                    // Ha denegado
                    Toast.makeText(getContext(), "Por favor configure los permisos de ubicacion para poder usar este modulo :::> ", Toast.LENGTH_LONG).show();
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_PERMISSION_CODE);
                }
            }

        }else{
            // Version inferiores a marshmallows
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse("package:" + getActivity().getPackageName()));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(intent);
        }

    }

    /**
     * <p>Actualiza localizacion</p>
     */
    private void updateLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 0, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 0, this);
    }

    private boolean checkPermission(String permission){
        int result = getActivity().checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){
            case LOCATION_PERMISSION_CODE:
                String permission  = permissions[0];
                int result = grantResults[0];
                if(permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)){
                    //Comprobar si ha sido aceptado el permiso
                    if(result == PackageManager.PERMISSION_GRANTED){
                        // Concedio el permiso
                        Log.d(LOG_TAG," Se dio el permiso a localizacion y se va a Instancia el location manager");
                        updateLocation();

                    }else{
                        // No concedio el permiso
                        Toast.makeText(getContext(),"No doy permiso",Toast.LENGTH_LONG).show();
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_PERMISSION_CODE);
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }
}
