package com.co.bog.branch.adapter.listener;

import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Documento;
import com.co.bog.branch.ws.models.Orden;
import com.co.bog.branch.ws.models.Vehiculo;

import java.util.List;

public interface SwitchFragmentListener {

    void  onSwitchVehicleFragment(Vehiculo vehiculo);

    void onSwitchVehicleDocumentosFragment(Vehiculo vehiculo);

    void onSwitchCitaFragment();

    void onSwitchGalleryFragment(Cita cita);

    void onSwitchDetailCita(Cita cita);

    void onSwitchVisualizadorPDF(Cita cita);

    void onSwitchUserFragment();

    void onSwitchDetailPhotos(Cita cita, int indexEtapa, int position);

}
