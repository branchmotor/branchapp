package com.co.bog.branch.ws.enums;

public enum OrdenEtapa {

    INGRESO("Ingreso", 2), DIAGNOSTICO("Diagnostico", 3), COTIZACION("Cotizacion",4),
    APROBACION("Aprobacion",5), REPARACION("Reparacion",6),ENTREGA("Entrega", 7) ;

    private final String key;
    private final Integer value;

    OrdenEtapa(String key, Integer value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }
    public Integer getValue() {
        return value;
    }
}
