package com.co.bog.branch.ws;

import android.content.Context;

import com.co.bog.branch.ws.deserializer.DeserializerCita;
import com.co.bog.branch.ws.deserializer.DeserializerVehicle;
import com.co.bog.branch.ws.interceptor.NetworkConnectionInterceptor;
import com.co.bog.branch.ws.models.Cita;
import com.co.bog.branch.ws.models.Vehiculo;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ws {

    public static final String BASE_URL = "http://abedc968.ngrok.io/";

    private static Retrofit retrofit= null;

    public static Retrofit getService(Context mContext){
        if(retrofit == null){

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                    .addInterceptor(new NetworkConnectionInterceptor(mContext))
                    .callTimeout(2, TimeUnit.MINUTES)
                    .connectTimeout(50, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS);



            /*
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                    .callTimeout(2, TimeUnit.MINUTES)
                    .connectTimeout(50, TimeUnit.SECONDS)
                    .readTimeout(4, TimeUnit.MINUTES)
                    .writeTimeout(4, TimeUnit.MINUTES);*/


            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Vehiculo.class, new DeserializerVehicle());
            builder.registerTypeAdapter(Cita.class, new DeserializerCita());

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create(builder.create()))
                    .build();
        }
        return retrofit;
    }
}
