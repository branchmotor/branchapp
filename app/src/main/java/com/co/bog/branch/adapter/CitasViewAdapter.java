package com.co.bog.branch.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.co.bog.branch.adapter.holder.CitaViewHolder;
import com.co.bog.branch.adapter.listener.OnItemClickListener;
import com.co.bog.branch.ws.models.Cita;

import java.util.List;

public class CitasViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String LOG_TAG = CitasViewAdapter.class.getSimpleName();

    private List<Cita> listCitas;

    private OnItemClickListener itemClickListener;

    private int layout;

    public CitasViewAdapter(List<Cita> listCitas, int layout, OnItemClickListener itemClickListener) {
        this.listCitas = listCitas;
        this.layout = layout;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        CitaViewHolder vh = new CitaViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((CitaViewHolder) holder).bind(this.listCitas.get(position), itemClickListener);
    }

    @Override
    public int getItemCount() {
        return this.listCitas.size();
    }


}
