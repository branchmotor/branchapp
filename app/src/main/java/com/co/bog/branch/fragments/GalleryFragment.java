package com.co.bog.branch.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.co.bog.branch.R;
import com.co.bog.branch.adapter.PhotoAdapter;
import com.co.bog.branch.adapter.listener.SwitchDetailGalleryFragmentListener;
import com.co.bog.branch.ws.enums.OrdenEtapa;
import com.co.bog.branch.ws.models.Documento;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryFragment extends Fragment{

    private static final String LOG_TAG = GalleryFragment.class.getSimpleName();

    private List<Documento> listPhotos = new ArrayList<>();

    private RecyclerView recyclerViewGallery;

    private RecyclerView.Adapter mAdapter;

    private RecyclerView.LayoutManager mLayoutManager;

    private static SwitchDetailGalleryFragmentListener switchFragmentlistener;

    private int idetapa;



    public GalleryFragment(List<Documento> listPhotos,int idetapa ,SwitchDetailGalleryFragmentListener switchDetailGalleryFragmentListener) {
        // Required empty public constructor
        this.listPhotos = listPhotos;
        this.idetapa = idetapa;
        this.switchFragmentlistener = switchDetailGalleryFragmentListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);

        this.recyclerViewGallery = (RecyclerView) view.findViewById(R.id.galleryRecycler);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setupRecyclerView(this.recyclerViewGallery);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {

        mLayoutManager = new GridLayoutManager(getContext(),1,RecyclerView.HORIZONTAL,false);

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);

        mAdapter = new PhotoAdapter(listPhotos, R.layout.photo_item, new PhotoAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int position) {

                if(idetapa == OrdenEtapa.DIAGNOSTICO.getValue()){
                    switchFragmentlistener.onSwitchDetailDiagnosticoPhotos(listPhotos,position);
                }

                if(idetapa == OrdenEtapa.REPARACION.getValue()){
                    switchFragmentlistener.onSwitchDetailReparacionPhotos(listPhotos,position);
                }




                /*pictureBrowserFragment browser = pictureBrowserFragment.newInstance(listDocumentos,position,getContext());

                // Note that we need the API version check here because the actual transition classes (e.g. Fade)
                // are not in the support library and are only available in API 21+. The methods we are calling on the Fragment
                // ARE available in the support library (though they don't do anything on API < 21)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    //browser.setEnterTransition(new Slide());
                    //browser.setExitTransition(new Slide()); uncomment this to use slide transition and comment the two lines below
                    browser.setEnterTransition(new Fade());
                    browser.setExitTransition(new Fade());
                }

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .addSharedElement(image, position+"picture")
                        .add(R.id.displayContainer, browser)
                        .addToBackStack(null)
                        .commit();*/
            }
        });

        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
